angular.module('drugsApp', ['infinite-scroll', 'angular-loading-bar', 'ngSanitize'], function ($locationProvider) {
    $locationProvider.html5Mode(true).hashPrefix('*');
})

.run(function ($rootScope, $location, pageType) {
    $location.search('kategori', null);
    $location.search('pabrik', null);
    $rootScope.$on('$locationChangeSuccess', function () {
        pageType();
    });
})
.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.parentSelector = '.navbar-default';
    cfpLoadingBarProvider.includeSpinner = false;
}])

.factory('pageType', function ($rootScope, $location) {
    return function () {
        arr = $location.path().split("/");
        params = {};
        switch(arr[1]) {
            case 'cari': 
                params.page = 'list-search'; 
                params.term = $location.search().q; 
                params.class = $location.search().kategori;
                params.manufacturer = $location.search().pabrik;
                break;
            case '': 
            case undefined: params.page = 'list-all'; break;
            default:
                params.page = 'single';
                params.slug = arr[1];
        }

        $rootScope.$broadcast('load-page', params);
    };
})
.factory('listFilter', ['$rootScope', function($rootScope) {
    return function (params) {
        $rootScope.$broadcast('list-filtered', params);
    };
}])
.factory('refUrl', ['$rootScope', function($rootScope) {
    return function (url) {
        $rootScope.$broadcast('ref-url-change', url);
    };
}])

.controller('searchCtrl', function($scope, $location, listFilter) {
    var vm = this;
    vm.is_searched = false;
    vm.is_show_tutorial = true;
    vm.filters = {
        term: $location.search().q,
        pabrik: {},
        kategori: {},
    };
    vm.search = function (e) {
        if (e) {
            if (e.which != 13) return;
            if (!vm.filters.term.length && e.which == 13) {
                $location.url("/");
                return;
            }
        }
        if (vm.filters.term.length < 3) return;
        $location.path("cari").search('q', vm.filters.term);
        vm.is_searched = true;
        vm.is_show_tutorial = false;
    };
    vm.clear_search = function () {
        vm.filters.term = '';
        vm.is_searched = false;
        $location.path("cari").search('q', null);
    };
    vm.filter = function (params) {
        listFilter(params);
    };
    vm.clear_filter = function (filter) {
        vm.filters[filter] = {};
        $location.search(filter, null);
    };
    $scope.$on('list-filtered', function (e, params) {
        vm.is_show_tutorial = false;
        vm.filters[params.type] = {label: params.label, value: params.value};
        $location.path("cari").search(params.type, vm.filters[params.type].value);
    });

    $scope.$on('load-page', function (e, params) {
        if (params.page != 'list-search') {
            is_searched = false;
            vm.filters = {
                term: '',
                pabrik: {},
                kategori: {},
            };
        }
    });
})

.controller('listCtrl', function ($http, $scope, $location, refUrl) {
    var vm = this;
    vm.is_loading = true;
    vm.is_finished = false;
    vm.drugs = [];
    vm.load = function (url, params, first) {
        if ((vm.is_finished && !first) || !vm.is_active)
            return;
        vm.is_loading = true;
        $http.get(url, {params: params})
            .then(function (response) {
                if (first) vm.drugs = response.data;
                else {
                    vm.drugs.next_page_url = response.data.next_page_url;
                    for (var i = 0; i <= response.data.data.length - 1; i++) {
                        vm.drugs.data.push(response.data.data[i]);
                    }
                }
                vm.is_finished = vm.drugs.next_page_url == null;
                vm.is_loading = false;
            }, function () {
                vm.is_loading = false;
                vm.has_error = true;
            });
    };
    vm.search = function (params) {
        vm.load(api_base_url + "api/drugs/search", params, true);
    };
    vm.next = function () {
        if (vm.is_loading) return;
        vm.load(vm.drugs.next_page_url);
    };
    vm.select_single = function (e, slug, name) {
        e.preventDefault();

        // To do: store referring url
        refUrl($location.url());
        $location.url(slug);
    };
    $scope.$on('load-page', function (e, params) {
        if (params.page == 'list-all') {
            vm.is_active = true;
            vm.load(api_base_url + "api/drugs/list/alpha", {}, true);
            return;
        }
        if (params.page == 'list-search') {
            vm.is_active = true;
            vm.search({
                q: params.term,
                class: params.class,
                manufacturer: params.manufacturer
            });
            return;
        }
        vm.is_active = false;
    });
})

.controller('singleCtrl', function ($scope, $http, $location) {
    var vm = this;
    vm.drug = {};
    vm.more_drugs = [];
    vm.more_drugs_limit_default = 5;
    vm.more_drugs_limit = vm.more_drugs_limit_default;
    vm.ref_url = "/";
    vm.dist_images = {
        'o': 'narotika.png',
        'g': 'obat-keras.jpg',
        'w': 'obat-bebas-terbatas.png',
        'b': 'obat-bebas.png'
    };
    vm.back = function () {
        $location.url(vm.ref_url);
    };

    $scope.$on('ref-url-change', function (e, url) {
        vm.ref_url = url;
    });
    $scope.$on('load-page', function (e, params) {
        if (params.page == 'single') {
            $http.get(api_base_url + "api/drug/" + params.slug)
                .then(function (response) {
                    vm.drug = response.data.drug;
                    vm.more_drugs = response.data.more_drugs;
                    vm.more_drugs_limit = vm.more_drugs_limit_default;
                    vm.is_active = true;
                });
            return;
        }
        vm.is_active = false;
    });
});