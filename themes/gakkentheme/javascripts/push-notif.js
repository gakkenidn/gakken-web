var config = {
    messagingSenderId: "337976016850"
};

firebase.initializeApp(config);

const messaging = firebase.messaging();

messaging.requestPermission()
.then(function(){
    console.log('have permission');
    return messaging.getToken();
})
.then(function(token){
    subscribeUser(token);
    sendToServer(token);
    saveToken(token);
})
.catch(function(err) {
    console.log('Permission Denied');
});

function subscribeUser(token) {

    fetch("https://iid.googleapis.com/iid/v1/" + token + "/rel/topics/general", {
        method: "POST" ,
        headers: new Headers({
            'Content-Type': 'application/json' ,
            'Authorization': 'key=AIzaSyDyb4gKmTMnQMcBWdd_Vdu5GvoWdtNuvUE'
        })
    })
    .then(function(response) {
        return response.json();
    })
    .then(function(data) {
    })
    .catch(function(err) {
        // console.log('There was a problem when fetching the data');
    });
}

function sendToServer(token) {
    const formData = new FormData();

    formData.append('token', token);

    let local_token = localStorage.getItem('token_fcm');

    if (local_token != token) {
        fetch(url_fcm, {
            method: "POST" ,
            mode: 'no-cors',
            body: formData
        })
        .then(function(response) {
            // console.log('success');
        })
        .catch(function(err) {
            console.log(err);
            // console.log('There was a problem when fetching the data');
        });
    }
}

function saveToken(token) {
    localStorage.setItem('token_fcm', token);
}

messaging.onMessage(function(payload) {
    // console.log('terload');
    // console.log("Message received. ", payload.notification);
});

messaging.onTokenRefresh(function() {
    messaging.getToken()
    .then(function(refreshedToken) {
        subscribeUser(token);
        sendToServer(token);
        saveToken(token);
    })
    .catch(function(err) {
        //error
    })
});
