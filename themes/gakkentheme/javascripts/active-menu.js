var gakkenNav       = document.getElementById('gakken-nav2') ,
    protocol        = window.location.protocol;
    host            = window.location.host;
    currLocation    = window.location.pathname ,
    loop            = true;

var el = null;

while (loop) {
    el = gakkenNav.querySelector("[href='" + protocol + '//' + host + currLocation +  "']");

    if (!el && currLocation.slice(currLocation.length-1) == '/') {
        currLocation = currLocation.slice(0, currLocation.length-1);
        el = gakkenNav.querySelector("[href='" + protocol + '//' + host + currLocation +  "']");
    }

    if (el) {
        resetActiveMenu();
        el.parentNode.classList.add('active');
        loop = false;

        if (el.parentNode.parentNode.parentNode &&
            el.parentNode.parentNode.parentNode.tagName == 'LI') {
                el.parentNode.parentNode.parentNode.classList.add('active');
        }
    }

    if (currLocation == '/' || currLocation == '') loop = false;

    currLocation = currLocation.split("/");

    currLocation.pop();

    if (currLocation.length == 0) loop = false;

    currLocation = currLocation.join('/');
    currLocation += '/';

}

function resetActiveMenu() {
    var elMenu = gakkenNav.querySelectorAll('li');

    elMenu.forEach(function(elm){
        elm.classList.remove('active');
    });
}
