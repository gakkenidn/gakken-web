var sliderJuonal    = document.getElementsByClassName('slider-journal') ,
    sliderWrap      = document.getElementsByClassName('slider-wrap')[0] ,
    navJournal      = document.getElementById('navJournal');

var prevActivate    = '0';
var id = null;

function initSlideJournal(){
    var indexNav = sliderWrap.offsetHeight / 240;

    navJournal.innerHTML = '';

    for (var i = 0; i < indexNav; i++) {
        var checked = (i === 0) ? 'checked' : '';

        var navButton = '<li>';
            navButton += '<input type="radio" name="journal_slider_nav" id="journal_slider_nav' + i + '" value="' + i + '" ' + checked + ' class="hide">';
            navButton += '<label for="journal_slider_nav' + i + '" class="slider-journal-dot-wrap">';
            navButton += '<div class="slider-journal-dot"></div>';
            navButton += '<span class="slider-journal-dot-active"></span>';
            navButton += '</label>';
            navButton += '</li>';

        navJournal.insertAdjacentHTML('beforeend', navButton);
    }

    var journalNav      = document.getElementsByName('journal_slider_nav');

    journalNav.forEach(function( item ){
        item.onclick = function(e) {
            if (this.value !== prevActivate) {
                if (this.value == '0') margin = 0;
                else margin = (this.value * -240);

                prevActivate = this.value;
                sliderWrap.style.marginTop = margin + 'px';
            }
        }
    });
}

initSlideJournal();

window.addEventListener("resize", function(e){
    initSlideJournal();
    sliderWrap.style.marginTop = '0px';
});
