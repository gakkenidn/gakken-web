angular.module('mnediadataApps', ['infinite-scroll', 'angular-loading-bar'])
.controller('mediadataCtrl', function($scope, $rootScope, $http) {
    var self = this;

    self.is_loading = false;
    self.is_finished = false;
    $scope.mediadata = {
        'keyword'   : null ,
        'appearance': '#' ,
        'sortby'    : 'newest'
    }

    self.load = function(url, keyword, appearance, sortby) {
        if (self.is_finished) return;

        keyword     = (keyword == '#') ? null : keyword;
        appearance  = (appearance == '#') ? null : appearance;
        sortby      = sortby || 'newest';

        self.is_loading = true;
        $http.post(url, {
            'keyword'       : keyword ,
            'appearance'    : appearance ,
            'orderby'        : sortby
        })
        .then(function(response) {
            if (response.data.current_page == '1') {

                self.mediadatas = response.data;

            } else {

                self.mediadatas.next_page_url = response.data.next_page_url;

                response.data.data.forEach(function (item) {
                    self.mediadatas.data.push(item);
                });
            }

            self.is_finished = self.mediadatas.next_page_url == null;
            self.is_loading = false;
        });
    }

    self.next = function () {
        if (self.is_loading) return;
        self.load(self.mediadatas.next_page_url, $scope.mediadata.keyword, $scope.mediadata.appearance, $scope.mediadata.sortby);
    }

    self.search = function (e, origin) {
        self.is_finished = false;

        if (origin == 'searchbox' || origin == 'btnsearch') {
            if (
                (
                    origin == 'searchbox' &&
                    ($scope.mediadata.keyword.length < 3  || e.keyCode != 13)
                ) ||
                (origin == 'btnsearch' && $scope.mediadata.keyword.length < 3)
            ) return;
        }

        history.pushState('', "Mediadata", "?");

        if ($scope.mediadata.keyword != null && $scope.mediadata.keyword.length > 0) {
            history.pushState('', "Mediadata", "?q=" + $scope.mediadata.keyword);
        }

        self.load(url_mediadata, $scope.mediadata.keyword, $scope.mediadata.appearance, $scope.mediadata.sortby);

    }

    self.removeKeyword = function () {
        $scope.mediadata.keyword = '';

        self.is_finished = false;
        history.pushState('', "Mediadata", "?");

        self.load(url_mediadata, '#', null, '#', '#');
    }

    self.callSingle = function(slug){
        $rootScope.$emit("requestSingle", slug);
    }

    var qvSearch = getQueryVariable('q');

    if (qvSearch) $scope.mediadata.keyword = qvSearch;

    self.load(url_mediadata, $scope.mediadata.keyword, $scope.mediadata.appearance, $scope.mediadata.sortby);
})
.controller('mediadatasingleCtrl', function($scope, $rootScope, $http, $q, $sce){
    var self = this;
    var canceler = $q.defer();

    self.is_loading = false;
    self.mediadata = null;
    self.show = false;

    $scope.trust = $sce.trustAsHtml;

    self.load = function(slug) {
        self.show = false;
        if (self.is_loading) canceler.resolve();

        self.is_loading = true;
        $http.get(url_mediadatasingle + '/' + slug)
        .then(function(response) {
            self.mediadata = response.data;
            self.is_loading = false;
            self.show = true;
        });
    }

    $rootScope.$on("requestSingle", function(e, slug){
        self.load(slug);
    });
});

function getQueryVariable (variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if(pair[0] == variable){return pair[1];}
    }

    return false;
}
