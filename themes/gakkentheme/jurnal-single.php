<?php
    get_header();
    $slug       = urldecode($wp_query->query_vars['journal']);
    $journal    = json_decode(do_shortcode('[gkvault-journals-single slug="' . $slug . '"]'));
    $vaulturl   = do_shortcode('[gkvault-get-base-url]');
    $account    = json_decode(do_shortcode('[gkaccount-login-status]'));
    $date_today = date('Y-m-d H:i:s');

    if (
        $account->status == 'authenticated'
    ) {
        if (!isset($account->subscription)) {
            $access_alert = 'subscription';
        } else {
            $start_account       = date('Ym', strtotime($account->subscription->starts_at));
            $expire_account      = date('Ym', strtotime($account->subscription->expired_at));

            if ($expire_account < $date_today) $access_alert = 'subscription';
            else $access_alert = 'yes';
        }
    } else {
        $access_alert = 'login';
    }
?>

    <div class="full-width container-fluid">

        <div class="main-container">

            <ol class="breadcrumb">
                <li><a href="<?= get_site_url() . "/jurnal" ?>">Jurnal</a></li>
                <li class="active"><?= $journal->title ?></li>
            </ol>

            <div class="article-wrapper journal">
                <aside class=" col-sm-4">
                    <div class="container-aside">
                        <img src="<?= $vaulturl . 'journal/files/image/' . $journal->image_url ?>">
                    </div>
                    <div class="container-aside">
                        <h2> ISSN </h2>
                        <ul>
                            <li> <?= $journal->issn ?> </li>
                        </ul>
                    </div>
                    <div class="container-aside">
                        <h2> E-ISSN </h2>
                        <ul>
                            <li> <?= !empty($journal->e_issn) ? $journal->e_issn : '-' ?> </li>
                        </ul>
                    </div>
                    <div class="container-aside">
                        <h2> Penerbit </h2>
                        <ul>
                            <li> <?= $journal->publisher ?> </li>
                        </ul>
                    </div>

                    <div class="container-aside">
                        <h2> Penyunting </h2>
                        <ul>
                            <li> <?= $journal->editor ?> </li>
                        </ul>
                    </div>

                    <div class="container-aside">
                        <?php if (str_replace('-', '0', $journal->publish_date) > 0 ): ?>
                            <h2>Dipublikasikan pada </h2>
                            <ul>
                                <li> <?= $journal->publish_date ?> </li>
                            </ul>
                        <?php endif; ?>
                    </div>

                    <div class="container-aside">
                        <?php if ( !empty($journal->ext_url) && $journal->ext_url != null): ?>
                            <?php
                                switch ($access_alert) {
                                    case 'subscription' :
                                        $action = "href='" . get_site_url() . "/berlangganan/'";
                                        break;
                                    case 'login' :
                                        $action = "href='#' data-toggle='modal' data-target='#signin'";
                                        break;
                                    default:
                                        $action = "href=\"" . $journal->ext_url . "\"";
                                        break;
                                }
                            ?>
                            <a <?= $action ?> class='btn btn-primary see-button'>
                                Baca Jurnal
                            </a>
                        <?php endif; ?>
                    </div>

                </aside>

                <div class="container col-sm-8">
                    <div class="top-container">
                        <div class="row">

                            <div class="descriptions col-sm-10">
                                <h1>
                                    <?= $journal->title ?>
                                </h1>
                            </div>

                        </div>
                    </div>

                    <div class="middle-container">
                        <h3>Deskripsi </h3>
                        <div class="extra">
                            <div>
                                <?= $journal->description ?>
                            </div>
                        </div>
                    </div>

                    <div class="middle-container">
                        <article>
                        <?php if ( empty($journal->ext_url) || $journal->ext_url == null): ?>
                            <h3> Artikel </h3>
                            <ul>
                                <?php foreach ($journal->articles as $article): ?>
                                    <?php
                                        switch ($access_alert) {
                                            case 'subscription' :
                                                $action = "href='" . get_site_url() . "/berlangganan/'";
                                                break;
                                            case 'login' :
                                                $action = "href='#' data-toggle='modal' data-target='#signin'";
                                                break;
                                            default:
                                                $action = "href=\"" . get_site_url() . "/jurnal/artikel/" . $article->slug . "\"";
                                                break;
                                        }
                                    ?>
                                    <li>
                                        <a <?= $action ?>>
                                            Volume <?= $article->journal_volume ?> - <?= $article->title ?>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
