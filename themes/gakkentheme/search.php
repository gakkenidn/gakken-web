<?php get_header() ?>
	
	<div class="full-width container-fluid">

		<div class="main-container" style="margin-top:20px;">

		<div class="row" style="margin-left:0;margin-right:0;">
			<div class="article-wrapper col-sm-9">
			
				<?php if (have_posts()): ?>

					<h1 class="super-title">Hasil pencarian artikel kesehatan "<?php echo $_GET['s'] ?>"</h1>

					<div class="all-article-container" style="margin-bottom:15px;">
						<?php require_once 'omnisearch.php'; ?>
					</div>
					<div class="all-article-container">

						<?php while(have_posts()): the_post(); ?>
							<div class="row">
								<div class="img col-sm-4 img-responsive">
									<?php the_post_thumbnail('medium'); ?>
								</div>

								<div class="container col-sm-8">
									<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

									<?php the_excerpt();?>

									<div class="sub-desc">
										<small>
											<?php the_date(); ?>
											<i style="color:#A41E22;font-weight:bold;">by</i>
											<?php the_author_posts_link(); ?>
										</small>
									</div>

								</div>
							</div>

						<?php endwhile; ?>
					</div>

				<?php endif; ?>
				
			</div>

			<?php get_sidebar() ?>
		</div>

		</div>
	</div>

<?php get_footer() ?>