<aside class= "sidebar-container col-sm-3">
	<?php if(false):?>
	<div>
		<h1 class="ellipsis sub-title"><span>Artikel Terpopuler</span></h1>
		<div class="main-banner popular-posts">
			<ul style="padding-top: 4px;">

				<?php
				$popularpost = new WP_Query([
					'posts_per_page' => 5,
					'ignore_sticky_posts' => 1,
					'meta_key' => 'wpb_post_views_count',
					'orderby' => 'meta_value_num',
					'order' => 'DESC'
				]);
				$i = 1;
				while ($popularpost->have_posts()):
					$popularpost->the_post();
					?>

					<li class="row">
						<div class="number-container">
							<a class="article-img" href="<?php the_permalink(); ?>" style="background:#A41E22 no-repeat;background-size:cover;background-position:50% 50%;width:80%;height:40px;display:block;text-align:center;color:#fff;padding:10px;margin-left:15px;">
								<span><?php echo $i; ?></span>
							</a>
						</div>

						<div class="headlines-container"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
					</li>

					<?php
					$i++;
				endwhile;
				?>
			</ul>
		</div>
	</div>
	<?php endif;?>

	<div>
		<h1 class="ellipsis sub-title"><span>Artikel Terpopuler</span></h1>
		<div class="main-banner popular-posts" id="popular-post">
		<?php
			$args = array(
	    		 'limit' => 5,
	    		 'range' => 'monthly',
	    		 'freshness' => 1,
	    		 'post_type' => 'post',
	    		 'order_by' => 'views',
	    		 'stats_views' => 0,
	    		 'wpp_start' => '<ul>',
	    		 'wpp_end' => '</ul>',
	    		 'post_html' => '<li class="row">
		    		 				<div class="number-container">
										<a class="article-img increment" href="{url}" style="background:#A41E22 no-repeat;background-size:cover;background-position:50% 50%;width:80%;height:40px;display:block;text-align:center;color:#fff;padding:10px;margin-left:15px;">

										</a>
									</div>
									<div class="headlines-container"><a href="{url}">{text_title}</a></div>
							</li>'
	    	);

			wpp_get_mostpopular($args);?>
		</div>

		<script type="text/javascript">
			var popularPostElm = document.getElementById('popular-post');
			var elmInc = popularPostElm.getElementsByClassName('increment');
			var i = 1;

			for (var index in elmInc) {
				if (elmInc.hasOwnProperty(index)) {
					elmInc[index].innerHTML = i;
					i++;
				}
			}

		</script>
	</div>


	<div style="padding:40px 0px;">
		<h1 class="ellipsis sub-title"><span>Artikel Pilihan</span></h1>
		<div class="main-banner popular-posts">
			<ul>
				<?php
				if (have_posts() && query_posts('showposts=4')):
					while (have_posts()):
						the_post();
						?>

						<?php if (is_sticky()): ?>
							<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full'); ?>
							<li class="row" style="margin:0 auto;">
								<div class="number-container">
									<a class="article-img" href="<?php the_permalink(); ?>" style="background:url('<?php echo $thumb['0']; ?>') no-repeat;background-size:cover;background-position:50% 50%;width:100%;height:70px;display:block;"></a>
								</div>
								<div class="headlines-container">
									<a href="<?php the_permalink();?>"><?php the_title(); ?></a>
								</div>
							</li>

							<?php
						endif;
					endwhile;
				endif;
				?>

			</ul>
		</div>
	</div>

	<div>
		<?php $downloads = get_post_meta(get_the_ID(), 'related_downloads'); ?>
		<?php if ($downloads[0] != null): ?>
			<h3>Unduhan</h3>
			<?php foreach($downloads as $dl): ?>
				<?php echo do_shortcode('[wpdm_package id="' . $dl['ID'] . '" template="link-template-default"]'); ?>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>

	<?php if(false): ?>
		<div class="ask-friend">
			<div class="title">
				<i class="fa fa-user-md"></i> Tanya Teman Sejawat
			</div>

			<div class="ask-wrap">
				<label for="ask">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br> <br> <br> Submit your questions:</label>
				<textarea id="ask">Put your question here</textarea>

				<input type="submit" value="Submit"> </input>
			</div>
		</div>
	<?php endif; ?>

	<?php dynamic_sidebar( $sidebar );?>




</aside>
