<?php
    get_header();

    if($wp_query->query_vars['journal'] != 'all'):
        $slug = urldecode($wp_query->query_vars['journal']);

        $journal = json_decode(do_shortcode('[gkvault-journals-single slug=' . $slug . ']'));
?>
    <!-- single journal -->
    <!-- judul journal -->
    <div>
        <?= $journal->title ?>
    </div>

    <div class="">
        <?= $journal->issn ?>
    </div>

    <div class="">
        <?= $journal->description ?>
    </div>

    <div class="">
        <?= $journal->publish_date ?>
    </div>

    <div class="">
        <?= $journal->publisher ?>
    </div>

    <div class="">
        <?= $journal->editor ?>
    </div>

    <!-- cover journal -->
    <div class="">
        <img src="" alt="" />
    </div>

    <ul>
        <?php foreach ($journal->article as $data): ?>
            <li><?= $data->title ?> || <?= $data->author ?></li>
        <?php endforeach; ?>
    </ul>
    <!-- end of single journal -->

<?php else: ?>
    <div class="full-width container-fluid">
        <div class="main-container" >
            <div class="row" style="margin-left: 0px; margin-right: 0px; margin-top: 20px;">
                <div class="article-wrapper col-sm-9">

                    <div class="all-article-container" id="journal-list">
                        <!-- journal will be dynamically using dom -->
                    </div>

                    <div class="load-more-section">
                        <button type="button" id="load-more-btn" class="hidden btn btn-default btn-load-more"><i class='fa fa-circle-o-notch fa-spin hidden' id='loading-content'></i> Load More</button>
                    </div>

                </div>

                <?php get_sidebar();?>
            </div>
        </div>
    </div>

    <?php do_shortcode('[gkvault-journals-list]'); ?>
<?php
    endif;
    get_footer();
?>
