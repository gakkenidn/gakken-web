<?php get_header(); ?>

    <div class="full-width container-fluid">

      <div class="main-container">

        <div class="article-wrapper journal">
            <aside class=" col-sm-4">
                <div class="container-aside">
                    <img src="images/jurnal.png">
                    <h2> Penulis </h2>
              <ul>
                <li>  C. Ronsmans, S. Scott, S.N. Qomariyah,
                      E. Achadi, D. Braunholtz, T. Marshall,
                      E. Pambudi, K.H. Wittenc & W.J. Graham </li>
              </ul>
            </div>

            <div class="container-aside">
              <h2>Dipublikasikan pada </h2>
              <ul>
                <li>  04/02/2009 </li>
              </ul>
            </div>

            <div class="container-aside">
              <h2> Unduh </h2>
              <ul>
                <li> <img src="images/pdf.png"> </li>
                <li>  Professional assistance during birth and maternal mortality in two Indonesian districts </li>
              </ul>
            </div>

          </aside>

          <div class="container col-sm-8">
            <div class="top-container">
              <div class="row">

              <div class="descriptions col-sm-10">
                <h1>Professional assistance during birth and
                  maternal mortality in two Indonesian
                  districts
                </h1>
              </div>
              </div>
            </div>

            <div class="middle-container">
              <h3>Abstract </h3>
              <div class="extra">

                <div>
                  <p> Objective To examine determinants of maternal mortality and assess the effect of
                      programmes aimed at increasing the number of births attended by health professionals
                      in two districts in West Java, Indonesia. </p><p>

                      Methods We used informant networks to characterize all maternal deaths, and a
                      capture-recapture method to estimate the total number of maternal deaths. Through a survey of recent births we counted all midwives practising in the two study districts. We used case–control analysis to examine determinants of maternal mortality, and cohort analysis to estimate overall maternal mortality ratios.</p><p>

                      Findings The overall maternal mortality ratio was 435 per 100 000 live births (95% confidence interval, CI: 376–498). Only 33% of women gave birth with assistance from a health professional, and among them, mortality was extremely high for those in the lowest wealth quartile range (2303 per 100 000) and remained very high for those in the lower middle and upper middle quartile ranges (1218 and 778 per 100 000, respectively). This is perhaps because the women, especially poor ones, may have sought help only once a serious complication had arisen.</p><p>

                      Conclusion Achieving equitable coverage of all births by health professionals is still a distant goal in Indonesia, but even among women who receive professional care, maternal mortality ratios remain surprisingly high. This may reflect the limitations of homebased care. Phased introduction of fee exemption and transport incentives to enable all women to access skilled delivery care in health centres and emergency care in hospitals may be a feasible, sustainable way to reduce Indonesia’s maternal mortality ratio </p>
                </div>
              </div>


              </div>
            </div>
          </div>

        </div>


        </div>
<?php get_footer(); ?>
