<?php
    /*Template Name : Single Content */
    $slug       = urldecode($wp_query->query_vars['content']);
    $content    = json_decode(do_shortcode('[gkvault-contents-single slug=' . $slug . ']'));
    $account    = json_decode( do_shortcode('[gkaccount-login-status]') );
    $date_today = date('Y-m-d H:i:s');
    $vaulturl   = do_shortcode('[gkvault-get-base-url]');
    $subscribe   = false;

    if ($account->status == 'authenticated' && $account->subscription) {
        $subscribe = $account->subscription->expired_at > $date_today;
    }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
  	<meta name="viewport" content="width=device-width">
    <meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
    <meta charset="utf-8">

    <?php wp_head();?>

    <title><?php
    global $page, $paged;

    wp_title(' | ', true, 'right');

    bloginfo('name');

    $site_description = get_bloginfo('description', 'display');
    if($site_description && (is_home() || is_front_page())) echo " | $site_description";

    if($paged >= 2 || $page >= 2)
    echo' | '.sprintf( __( 'Page %s', 'shape' ), max( $paged, $page ) );

    ?></title>


    <!-- Latest compiled and minified CSS -->

    <link rel="profile" href="http://gmpg.org/xfn/11">
  	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
    <![endif]-->

    <link href="<?php bloginfo('stylesheet_url'); ?>" media="screen" rel="stylesheet" type="text/css" />
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/stylesheets/print.css" media="print" rel="stylesheet" type="text/css" />
    <!-- <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/stylesheets/screen.css" media="screen" rel="stylesheet" type="text/css" /> -->
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/stylesheets/ie.css" media="print" rel="stylesheet" type="text/css" />
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/stylesheets/journal-slider.css" media="screen" rel="stylesheet" type="text/css" />
    <link rel=”alternate” type=”application/rss+xml” title=”RSS
    2.0” href=”<?php bloginfo('rss2_url'); ?>” />
    <link rel="shortcut icon" href="<?php bloginfo('template_directory');?>/images/favicon.ico" />


  </head>
  <body <?php body_class( isset($class) ? $class : '' ); ?> data-spy="scroll" data-target=".topics-tab" data-offset="50">

<div class="full-width container-fluid">
    <div class="main-container" >
        <div class="single-article-container" style="margin-top: 20px;">
            <?php if ($subscribe || $content->label): ?>

                <h2 class="text-center" style="margin-bottom:30px;"> <?= $content->title ?> </h2>

                <!-- content with embed data -->
                <?php if ( $content->type == 'embed'): ?>
                    <iframe src="<?= $content->data ?>" width="854px" height="480px"></iframe>

                <!-- conent with text data -->
                <?php elseif ( $content->type == 'text' ): ?>
                    <?= $content->data ?>

                <!-- content with file  data -->
                <?php elseif ( $content->type == 'file'): ?>

                    <!-- file apeearance as a pdf -->
                    <?php if ( $content->appearance == 'text' ): ?>
                        <object data="<?php echo do_shortcode('[gkvault-get-base-url]'); ?>/content/files/<?= $content->data ?>" style="width:100%;min-height:768px;"></object>

                    <!-- file type is image jpg, png, etc -->
                    <?php elseif( $content->appearance == 'image' ): ?>
                        <img src="<?= $vaulturl . '/content/files/' . $content->data ?>" alt="" />

                    <!-- file type is audio mp3, ogg etc -->
                    <?php elseif( $content->appearance == 'audio' ): ?>
                        <audio controls>
                            <source src="<?= $vaulturl . '/content/files/' . $content->data ?>" type="audio/mpeg">
                        </audio>

                    <!-- fil etype is video mp4, avi, etc -->
                    <?php elseif( $content->appearance == 'video' ): ?>
                        <video src="<?= $vaulturl . '/content/files/' . $content->data ?>"></video>
                    <?php endif; ?>

                <?php endif; ?>

            <?php else: ?>
                * Berlangganan <a href='<?= bloginfo('url') ?>/berlangganan'>disini</a>, untuk mengakses konten ini *
            <?php endif; ?>
        </div>
    </div>
</div>

</body>
</html>
