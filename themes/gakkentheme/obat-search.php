<?php
    /*Template Name : Obat search page */

    get_header();
    // obat search page
    ?>
    <div class="full-width container-fluid">

        <div class="main-container">
            <div class="article-wrapper col-lg-8">
            	<br />
            	<form class="search" accept-charset="utf-8" method="get" action="<?php bloginfo('url'); ?>/obat/search">
                	<div class="input-group stylish-input-group">
	                	<input type="text" name="term" class="form-control input-lg" placeholder="Cari obat.." value="<?= $_GET['term'] ?>" />
	                	<span class="input-group-addon">
	                    	<button type="submit">
	                        	<span class="fa fa-search"></span>
	                    	</button>
	                	</span>

	                </div>
                </form>

                <div class="article-container">
                	<?php do_shortcode("[gkvault-drugs-search term='" . $_GET['term'] . "']"); ?>
                </div>

        	</div>
        </div>

    </div>

	<?php
get_footer();
?>