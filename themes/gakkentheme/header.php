<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width,initial-scale=1" />
		<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
		<meta charset="utf-8" />

		<?php
		$title = "";
		if (!is_home()) {
			if (is_search()) $title = get_search_query();
			elseif (is_category()) $title = single_cat_title('', false);
			elseif (is_tax()) $title = single_cat_title('', false);
			else $title = get_the_title();
		}
		?>

		<title><?php if(!empty($title)): echo $title.' | Gakken Indonesia '; else: bloginfo('site_title'); endif; ?> </title>


		<?php if (isset($wp_query->query_vars['drug'])): ?>
			<base href="/obat/" />
		<?php endif ?>

		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<!--[if lt IE 9]>
		<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
		<![endif]-->

		<link href="<?php echo esc_url(get_template_directory_uri()); ?>/style.170522.css" media="screen" rel="stylesheet" type="text/css" />
		<link href="<?php echo esc_url(get_template_directory_uri()); ?>/stylesheets/print.css" media="print" rel="stylesheet" type="text/css" />
		<link href="<?php echo esc_url(get_template_directory_uri()); ?>/stylesheets/ie.css" media="print" rel="stylesheet" type="text/css" />
		<link href="<?php echo esc_url(get_template_directory_uri()); ?>/stylesheets/journal-slider.css" media="screen" rel="stylesheet" type="text/css" />
		<link href="<?php echo esc_url(get_template_directory_uri()); ?>/stylesheets/loading-bar.css" media="screen" rel="stylesheet" type="text/css" />
		<link href="<?php echo esc_url(get_template_directory_uri()); ?>/stylesheets/journal.css" media="screen" rel="stylesheet" type="text/css" />
		<link href="<?php echo esc_url(get_template_directory_uri()); ?>/stylesheets/mediadata.css" media="screen" rel="stylesheet" type="text/css" />
		<link rel="alternate" type="application/rss+xml" title="RSS	2.0" href="<?php bloginfo('rss2_url'); ?>" />
		<link rel="shortcut icon" href="<?php bloginfo('template_directory');?>/images/favicon.ico" />
		<script src="https://www.gstatic.com/firebasejs/3.5.2/firebase-app.js"></script>
		<script src="https://www.gstatic.com/firebasejs/3.5.2/firebase-messaging.js"></script>
		<link rel="manifest" href="/manifest.json">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script>
			window.fbAsyncInit = function() {
				FB.init({
					appId      : '1060121160748989',
					xfbml      : true,
					version    : 'v2.6'
				});
			};

			(function(d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) { return; }
				js = d.createElement(s);
				js.id = id;
				js.src = "//connect.facebook.net/en_US/sdk.js";
				fjs.parentNode.insertBefore(js, fjs);
			} (document, 'script', 'facebook-jssdk'));
		</script>

		<?php wp_head(); ?>

	</head>
	<body <?php body_class(isset($class) ? $class : ''); ?> data-spy="scroll" data-target=".topics-tab" data-offset="50">

		<?php do_shortcode('[gakken-signin]'); ?>

		<div class="full-container static-nv-container">
			<div class="full-width">
				<nav class="navbar navbar-default static-nv">
					<div class="container-fluid">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#gakken-nav2" aria-expanded="false">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<div class="logo">
								<a class="navbar-brand" href="<?php bloginfo('url'); ?>">
									<img src="<?php echo esc_url( get_template_directory_uri()); ?>/images/logo-gakken-indonesia.png" />
								</a>
							</div>
						</div>

						<div class="collapse navbar-collapse" id="gakken-nav2">
							<ul class="nav navbar-nav">
								<?php create_bootstrap_menu("primary"); ?>
							</ul>

							<?php do_shortcode('[gakken-account]'); ?>
						</div>
					</div>
				</nav>
			</div>
		</div>
