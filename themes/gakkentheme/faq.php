                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1">
                                            <i class="fa fa-caret-right"> </i>  Q1: Bagaimana saya bisa mengakses topik?
                                        </a>
                                    </div>

                                    <div id="collapse1" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            Topik hanya dapat diakses oleh member yang telah melakukan pendaftaran dan pembayaran untuk berlangganan layanan Gakken P2KB.
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse2">
                                            <i class="fa fa-caret-right"> </i> Q2: Apa yang akan saya dapatkan ketika sudah berlangganan layanan Gakken P2KB? 
                                        </a>
                                    </div>

                                    <div id="collapse2" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            Anda memiliki akses penuh di website Gakken Indonesia atas semua topik pembelajaran yakni berupa video dan teks, artikel dan jurnal medis, indeks obat, dokter, rumah sakit dan klinik.
                                    </div>
                                </div>

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse3">
                                            <i class="fa fa-caret-right"> </i> Q3: Apakah ada sesi trial sebelum membeli layanan penuh? 
                                        </a>
                                    </div>

                                    <div id="collapse3" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            Ya. Tersedia Konten Trial yang bisa diakses tanpa keanggotaan. Hubungi contact support kami untuk mendapatkan akses trial. support@gakken-idn.co.id atau telepon di 0800-1-401652 (Toll Free)
                                    </div>
                                </div>

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse4">
                                            <i class="fa fa-caret-right"> </i> Q4: Berapa lama masa berlangganan?
                                        </a>
                                    </div>

                                    <div id="collapse4" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            Masa berlangganan layanan Gakken P2KB yaitu selama 365 hari atau 12 bulan terhitung dari mulai masa aktif berlangganan. 
                                    </div>
                                </div>

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse5">
                                            <i class="fa fa-caret-right"> </i> Q5: Kapan topik terbaru rilis?
                                        </a>
                                    </div>

                                    <div id="collapse5" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            Topik pembelajaran terbaru dirilis pada tanggal 1 setiap bulannya. 
                                    </div>
                                </div>

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse6">
                                            <i class="fa fa-caret-right"> </i> Q6: Kapan sertifikat bisa diunduh?
                                        </a>
                                    </div>

                                    <div id="collapse6" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            Sertifikat otomatis dapat diunduh ketika anda telah menyelesaikan tes dengan presentasi kelulusan minimal 80%. 
                                    </div>
                                </div>

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse7">
                                            <i class="fa fa-caret-right"> </i> Q7: Bagaimana jika saya sudah mengikuti tes tapi hasilnya belum mencapai standar kelulusan 80%? 
                                        </a>
                                    </div>

                                    <div id="collapse7" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            Anda bisa mengikuti tes berkali-kali hingga mencapai standar kelulusan selama masih dalam masa berlangganan. 
                                    </div>
                                </div>

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse8">
                                            <i class="fa fa-caret-right"> </i> Q8: Bisakah saya mengunduh konten yang ada di dalam topik pembelajaran?
                                        </a>
                                    </div>

                                    <div id="collapse8" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            Anda hanya bisa mengakses semua topik yang telah dirilis selama terkoneksi dengan internet. Konten yang ada tidak bisa diunduh. 
                                    </div>
                                </div>