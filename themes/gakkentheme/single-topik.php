<?php
    /*Template Name : Topik page */
    get_header();
 ?>
<?php
    $slug       = urldecode($wp_query->query_vars['topic']);
    $account    = json_decode(do_shortcode('[gkaccount-login-status]'));

    $topic      = json_decode(do_shortcode('[gkvault-topics-single slug=' . $slug . ']'));
    $date_today = date('Ym');
    $vaulturl   = do_shortcode('[gkvault-get-base-url]');
    $backnumber = false;

    if (
        $account->status == 'authenticated'
    ) {
        if (!isset($account->subscription)) {
            $access_alert = 'subscription';
        } else {
            $access_alert = 'yes';
        }
    } else {
        $access_alert = 'login';
    }
?>
<div class="full-width container-fluid">
    <div class="main-container" style=" margin-top: 20px; ">
        <ol class="breadcrumb">
            <li><a href="<?= get_site_url() . "/topik" ?>">Topik</a></li>
            <li class="active"><?= $topic->title ?></li>
        </ol>
        <aside class= "sidebar-container topics-only col-sm-3">
            <div class="topics-tab" id="sidebar-scroll" data-spy="affix" data-offset-top="50">
                <div class="list-group" role="tablist">
                    <a href="#overview" class="list-group-item">
                        <span class="description"> Rangkuman </span>
                        <span class="fa fa-caret-right"> </span>
                    </a>
                    <a href="#lecture" class="list-group-item">
                        <span class="description"> Pengajar </span> <span class="fa fa-caret-right"> </span>
                    </a>
                    <?php if (!empty($topic->preview)): ?>
                        <a href="#preview" class="list-group-item">
                            <span class="description"> Preview </span> <span class="fa fa-caret-right"> </span>
                        </a>
                    <?php endif; ?>
                    <a href="#content-summary" class="list-group-item">
                        <span class="description"> Konten <span class="fa fa-caret-right"> </span> </span>
                    </a>
                    <a href="#achievements" class="list-group-item">
                        <span class="description"> Benefit <span class="fa fa-caret-right"> </span> </span>
                    </a>
                    <a href="#topic-faq" class="list-group-item">
                        <span class="description"> FAQs </span> <span class="fa fa-caret-right"> </span>
                    </a>
                    <?php if( $access_alert === 'subscribe' ): ?>
                        <div class="list-group-item">
                            <span class="description">
                                <a class="btn btn-lg btn-primary" href="<?= get_site_url('url') ?>/berlangganan"> Subscribe </a>
                            </span>
                        </div>
                    <?php elseif( $access_alert === 'backnumber' ): ?>
                        <div class="list-group-item">
                            <span class="description">
                                <a class="btn btn-lg btn-primary" href="<?= get_site_url('url') ?>/berlangganan"> Beli </a>
                            </span>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </aside>

        <div class="article-wrapper col-sm-9">
            <div class="single-article-container topic-article" style="overflow: hidden;">
                <h1 class="title"> <?= $topic->title ?> </h1>

                <section id="overview" class="jumptarget">
                    <h2 class="divider"> Rangkuman </h2>
                    <article>
                        <?= $topic->summary ?>
                    </article>
                </section>

                <section id="lecture" class="jumptarget">
                    <h2 class="divider"> Pengajar </h2>
                    <div class="taught-by col-sm-12">
                        <h4> Mentor  </h4>
                        <?php foreach ($topic->lectures as $lecture): ?>
                            <div class="taught-by-wrapper">
                                <div class="taught-by-desc ">
                                    <h3> <?= $lecture->front_title . ' ' . $lecture->name . (!empty($lecture->back_title) ? ', ' . $lecture->back_title : '') ?> </h3>
                                    <?= $lecture->bio ?>
                                </div>

                                <?php if ($lecture->photo): ?>
                                    <div class="taught-by-pic ">
                                        <div
                                            class="taught-by-pic-container"
                                            style="background: url('<?= $vaulturl ?>/lecture/photo/<?= $lecture->photo ?>') no-repeat; background-size: cover; background-position: 50% 50%;">
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </section>

                <?php if (!empty($topic->preview)): ?>
                    <section id="preview" class="jumptarget">
                        <h2 class="divider"> Preview </h2>

                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="<?= $topic->preview ?>" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </section>
                <?php endif; ?>

                <section id="content-summary" class="jumptarget" style="padding-bottom: 40px;">
                    <h2 class="divider"> Konten </h2>

                    <div class="panel panel-default">
                        <div class="list-group">
                                <?php foreach ($topic->content as $content):
                                    switch ($content->appearance) {
                                        case 'image'    : $icon = '<i class="fa fa-fw fa-file-image-o"> </i>'; break;
                                        case 'video'    : $icon = '<i class="fa fa-fw fa-play"> </i>'; break;
                                        case 'audio'    : $icon = '<i class="fa fa-fw fa-headphones"> </i>'; break;
                                        case 'quiz'     : $icon = '<i class="fa fa-fw fa-question"> </i>'; break;
                                        case 'download' : $icon = '<i class="fa fa-fw fa-download"> </i>'; break;
                                        default         : $icon = '<i class="fa fa-fw fa-file-text-o"> </i>'; break;
                                    }
                                    switch ($access_alert) {
                                        case 'subscription' :
                                            $action = "href='" . get_site_url() . "/berlangganan/'";
                                        break;
                                        case 'login' :
                                            $action = "data-toggle='modal' data-target='#signin'";
                                        break;
                                        default:
                                            $action = "onclick=\"window.open('" . get_site_url(null, 'topik/' . $topic->slug . '/konten/' . $content->slug) . "', 'contentWindow', 'resizable,scrollable,status,chrome=yes,centerscreen,width=948,height=700');return false;\"";
                                        break;
                                    }

                                    if ($access_alert == 'yes') {
                                        if ( $content->type === 'link') {
                                            if (strpos($content->data, 'https://p2kb.gakken-idn.id/moodle/mod/') == 0)
                                                $action = "onclick=\"window.open('https://p2kb.gakken-idn.id/moodle/auth/gakken/sso.php?redirect=" . urlencode($content->data) . "', 'p2kbwindow', 'resizable,scrollable,status,chrome=yes,centerscreen,width=948,height=700');return false;\"";
                                            else if($content->is_free == 1)
                                                $action = "onclick=\"window.open('" . urlencode($content->data) . "', 'contentWindow', 'resizable,scrollable,status,chrome=yes,centerscreen,width=948,height=700');return false;\"";
                                        } else {
                                            // $action = $subscription_status ? get_site_url(null, 'topik/' . $topic->slug . '/konten/' . $content->slug) : '#';
                                        }
                                    }

                                    if ($content->type == 'file' && ($content->is_free == 1 || $access_alert == 'yes')) {
                                        $action = "onclick=\"window.open('" . $vaulturl . 'content/files/' . $content->data . "', 'contentWindow', 'resizable,scrollable,status,chrome=yes,centerscreen,width=948,height=700');return false;\"";
                                    }

                                    if ($content->label == 'rangkuman' && ($content->is_free == 1 || $access_alert == 'yes')) {
                                        $action = "onclick=\"window.open('" . get_site_url(null, 'topik/' . $topic->slug . '/konten/' . $content->slug) . "', 'contentWindow', 'resizable,scrollable,status,chrome=yes,centerscreen,width=948,height=700');return false;\"";
                                    }

                                ?>
                                    <a <?= $action ?> style="cursor: pointer;" class="list-group-item">
                                        <div class="row">
                                            <div class="col-xs-1 text-center">
                                                <?= $icon ?>
                                            </div>
                                            <div class="col-xs-11">
                                                <?= ucfirst($content->label) ?>
                                            </div>
                                        </div>
                                    </a>
                                <?php endforeach; ?>

                        </div>

                    </div>
                </section>

                <?php if ($topic->is_p2kb): ?>
                    <section id="achievements" class="jumptarget">
                        <h2 class="divider"> Benefit </h2>
                        <div class="achievements-container col-sm-12">
                            <div class="achivements-item col-sm-4">
                                <img src="<?= get_bloginfo('template_directory') ?>/images/sertificates_02.png">
                                <p class="description">
                                    Sertifikat Keikutsertaan yang dapat dikonversi menjadi Sertifikat Kompetensi setelah menyelesaikan test dengan nilai kelulusan 80&percnt; atau lebih.
                                </p>
                            </div>
                        </div>
                    </section>
                <?php endif; ?>

                <section id="topic-faq" class="jumptarget">
                    <h2 class="divider"> Frequently Asked Questions </h2>

                    <div class="accordion" id="accordion2">
                        <?php $i = 1; foreach ($topic->faq as $data_faq): ?>
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse<?= $i ?>">
                                        <i class="fa fa-caret-right"> </i>  <?= $data_faq->question ?>
                                    </a>
                                </div>

                                <div id="collapse<?= $i ?>" class="accordion-body collapse">
                                    <div class="accordion-inner">
                                        <?= $data_faq->answer ?>
                                    </div>
                                </div>
                            </div>
                        <?php $i++; endforeach; ?>

                        <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1">
                                            <i class="fa fa-caret-right"> </i>  Q1: Bagaimana saya bisa mengakses topik?
                                        </a>
                                    </div>

                                    <div id="collapse1" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            Topik hanya dapat diakses oleh member yang telah melakukan pendaftaran dan pembayaran untuk berlangganan layanan Gakken P2KB.
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse2">
                                            <i class="fa fa-caret-right"> </i> Q2: Apa yang akan saya dapatkan ketika sudah berlangganan layanan Gakken P2KB?
                                        </a>
                                    </div>

                                    <div id="collapse2" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            Anda memiliki akses penuh di website Gakken Indonesia atas semua topik pembelajaran yakni berupa video dan teks, artikel dan jurnal medis, indeks obat, dokter, rumah sakit dan klinik.
                                    </div>
                                </div>

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse3">
                                            <i class="fa fa-caret-right"> </i> Q3: Apakah ada sesi trial sebelum membeli layanan penuh?
                                        </a>
                                    </div>

                                    <div id="collapse3" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            Ya. Tersedia Konten Trial yang bisa diakses tanpa keanggotaan. Hubungi contact support kami untuk mendapatkan akses trial. <strong>support@gakken-idn.co.id </strong> atau telepon di <strong> 0800-1-401652 (Toll Free) </strong>
                                    </div>
                                </div>

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse4">
                                            <i class="fa fa-caret-right"> </i> Q4: Berapa lama masa berlangganan?
                                        </a>
                                    </div>

                                    <div id="collapse4" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            Masa berlangganan layanan Gakken P2KB yaitu selama 365 hari atau 12 bulan terhitung dari mulai masa aktif berlangganan.
                                    </div>
                                </div>

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse5">
                                            <i class="fa fa-caret-right"> </i> Q5: Kapan topik terbaru rilis?
                                        </a>
                                    </div>

                                    <div id="collapse5" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            Topik pembelajaran terbaru dirilis pada tanggal 1 setiap bulannya.
                                    </div>
                                </div>

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse6">
                                            <i class="fa fa-caret-right"> </i> Q6: Kapan sertifikat bisa diunduh?
                                        </a>
                                    </div>

                                    <div id="collapse6" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            Sertifikat otomatis dapat diunduh ketika anda telah menyelesaikan tes dengan presentasi kelulusan minimal 80%.
                                    </div>
                                </div>

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse7">
                                            <i class="fa fa-caret-right"> </i> Q7: Bagaimana jika saya sudah mengikuti tes tapi hasilnya belum mencapai standar kelulusan 80%?
                                        </a>
                                    </div>

                                    <div id="collapse7" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            Anda bisa mengikuti tes berkali-kali hingga mencapai standar kelulusan selama masih dalam masa berlangganan.
                                    </div>
                                </div>

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse8">
                                            <i class="fa fa-caret-right"> </i> Q8: Bisakah saya mengunduh konten yang ada di dalam topik pembelajaran?
                                        </a>
                                    </div>

                                    <div id="collapse8" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            Anda hanya bisa mengakses semua topik yang telah dirilis selama terkoneksi dengan internet. Konten yang ada tidak bisa diunduh.
                                        </div>
                                    </div>
                                </div>

                    </div>
                </section>
            </div>
        </div>
    </div>
</div>


<?php
    get_footer();
?>
