<form class="omnisearch" accept-charset="utf-8" action="<?php bloginfo('url') ?>" method="get">
    <div class="input-group input-group-lg">
        <input type="text" name="s" class="form-control input-lg"<?php if(isset($_GET['s'])): ?> value="<?php echo $_GET['s'] ?>"<?php endif ?> placeholder="Cari artikel, jurnal, obat dan lainnya" />
        <?php
        // Search type selection should only appear at home page
        if (is_home()): 
            ?>
            <div class="input-group-btn">
                <button type="button" class="btn btn-default btn-lg dropdown-toggle" data-search="type" data-toggle="dropdown"><span data-search="type-label">Artikel</span> <b class="fa fa-fw fa-angle-down"></b></button>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#" data-search-type="artikel">Artikel</a></li>
                    <li><a href="#" data-search-type="jurnal">Jurnal</a></li>
                    <li><a href="#" data-search-type="obat">Obat</a></li>
                </ul>
            </div>
        <?php endif ?>
        <div class="input-group-btn">
            <button type="submit" class="btn btn-default btn-lg"><b class="fa fa-fw fa-search"></b></button>
        </div>
    </div>
</form>

<?php if (!is_search()): ?>
    <script type="text/javascript">
        var searchActions = {
            artikel: { 
                url: '<?php bloginfo('url') ?>',
                param: 's'
            },
            jurnal: { 
                url: '<?php bloginfo('url') ?>/jurnal',
                param: 'q'
            },
            obat: {
                url: '<?php bloginfo('url') ?>/obat/cari',
                param: 'q'
            }
        };
        function setSearch(elem) {
            var form = $(".omnisearch");
            form.attr('action', searchActions[elem.data('search-type')].url);
            form.find('input').attr('name', searchActions[elem.data('search-type')].param);
            form.find('[data-search=type-label]').text(elem.text());
        }
        $(document).ready(function () {
            $("[data-search-type]").on('click', function (e) {
                e.preventDefault();
                setSearch($(this));
            });
        });
    </script>
<?php endif ?>
