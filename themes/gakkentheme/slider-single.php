<div class="slider-container">

  <section class="slide-wrapper">
    <div class="slider">

     <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/javascripts/slider.js"></script>

     <div class="ninja-wrap">
        <section class="slider-cont">

          <?php

          if(is_category()):
            $cat = get_category( get_query_var( 'cat' ) );
            $category = $cat->cat_ID;

            $query = new WP_Query( array(
              'posts_per_page' => 5,
              // 'post__in' => get_option( 'sticky_posts'),
              'cat' => $category
              )
            );

          elseif(is_author()):

            $query = new WP_Query( array(
              'author_name' => get_the_author_meta('display_name'),
              // 'post__in' => get_option( 'sticky_posts'),
              'posts_per_page' => 5
              
              )
            );

          else:

            $query = new WP_Query( array(
              'posts_per_page' => 5,
              'post__in' => get_option( 'sticky_posts')
              )
            );

          endif;

           if( $query->have_posts() ) : while( $query -> have_posts() ) : $query->the_post();
          ?>
              <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
              
             <div class="slide" style="overflow:hidden; " >
                           <a class="slide-a" href="<?php the_permalink();?>" alt="<?php the_title();?>" style="display:block; background-image: url('<?php echo $thumb['0'];?>');
    background-size: cover; background-repeat: no-repeat;
    background-position: center; height: 100%;
    width: 100%; ">
                           
                           </a>

                 <div class="container tops">
                    <section class="row main-width">
                        <section class="slide-copy">
                          <article>
                            <h1><a href="<?php the_permalink();?>" alt="<?php the_title();?>"> <?php the_title();?> </a> </h1>

                              <?php $subheading = get_post_meta($post->ID, 'subheading', true); ?>
                                    <?php if (!empty($subheading)): ?>
                                  <div class="post-subheading">
                                    <?php echo apply_filters('the_excerpt', $subheading); ?>
                                  </div>
                                  <p style="text-align: right; "> <a href="<?php the_permalink();?>" style="color: #A41E22;"> Continue Reading <i class="fa fa-arrow-right"> </i></a> </p>
                                  <?php endif; ?>


                            </article>
                            </section>
                   </section>
               </div>
             </div>


           <?php endwhile; endif; ?>

        </section>
     </div>

      <div class="slider-nav main-width">
      <div class="slider-nav-wrapper">
        <ul class="slider-dots">
           <li><a href="#" class="arrow-prev"><i class="fa fa-angle-left"></i></a></li>

           <?php if( $query->have_posts() ) : while( $query -> have_posts() ) : $query->the_post();
           ?>
              <li class="dot"><i class="fa fa-circle fa-1"></i></li>

          <?php endwhile; endif;?>

          <li> <a href="#" class="arrow-next"><i class="fa fa-angle-right"></i></a></li>
        </ul>
      </div>
      </div>
      </div>
  </section>
</div>
