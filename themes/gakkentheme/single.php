<?php get_header(); ?>

<div class="full-width container-fluid">

	<div class="main-container" style="margin-top:20px;">

		<?php
		if(have_posts()):
			while(have_posts()):
				the_post();
				wpb_set_post_views(get_the_ID());

				$categories = get_the_category($post->ID);
				$is_artikel_opini = false;
				$cat_html = "";
				foreach ($categories as $cat) {
				    if ($cat->slug == 'opini') $is_artikel_opini = true;
				    else $cat_html .= '<a href="' . get_term_link($cat->term_id) . '">' . $cat->name . '</a>';
				}
				?>
				<div class="article-wrapper col-sm-9">
					<div class="single-article-container limit-width">

						<?php if ($is_artikel_opini): ?>
							<div class="badge-opini text-center" style="position:absolute;top:-5px;right:8%;padding:10px 15px;background:#A41E22;text-transform:uppercase;box-shadow:0 2px 2px rgba(0,0,0,0.2);color:#fff;border-bottom-left-radius:4px;border-bottom-right-radius:4px;">
							    <b class="fa fa-fw fa-star fa-2x"></b><br />
							    Opini
							</div>
							<div class="page-header" style="margin-right:16%;">
								<h1 class="title text-left"><?php the_title(); ?></h1>
								<div class="sub-desc text-left">
									<small>
										<?php the_date(); ?> by <?php the_author_posts_link(); ?>.
									</small>
								</div>
								<div class="category-wrap" style="overflow:hidden;">
									<?php echo $cat_html ?>
								</div>
							</div>
						<?php else: ?>
							<div class="page-header">
								<h1 class="title"><?php the_title(); ?></h1>
								<div class="sub-desc">
									<small>
										<?php the_date(); ?> by <?php the_author_posts_link(); ?>.
									</small>
								</div>
								<div class="category-wrap" style="overflow:hidden;">
									<?php echo $cat_html ?>
								</div>
							</div>
						<?php endif; ?>

						<article>
							<center><?php the_post_thumbnail('large'); ?></center>
							<div class="single-content">
							<?php the_content(); ?>
							</div>
						</article>

						<?php
				  		endwhile;
			  			endif;
			  		?>

						<?php
						$tags = get_the_tags(get_the_ID(), 'post_tag'); 
						$html = '<div class="post_tags">';
						if ($tags)
							foreach ($tags as $tag) {
								$tag_link = get_tag_link($tag->term_id);
								$html .= "<a href='{$tag_link}' title='{$tag->name} Tag' class='{$tag->slug}'>";
								$html .= "{$tag->name}</a>";
							}
						$html .= '</div>';
						echo $html;
						?>

						<?php
						$relateds = get_post_meta(get_the_ID(), 'related_posts');
						if ($relateds[0] != null):
							?>
							<div class='related-posts row'>
								<h3 style="font-weight:600;padding:15px;">Baca Juga</h3>
								<?php foreach($relateds as $article): ?>
									<div class=" article-row col-xs-4 col-sm-4 ">
										<div class="article-cont">
											<?php $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($article['ID']), 'full'); ?>
											<a class="article-img" href="<?php echo get_the_permalink($article['ID']); ?>" style="background:url('<?php echo $thumb['0'];?>') no-repeat;background-size:cover;background-position:50% 50%;width:100%;height:200px;display:block;">
											</a>

											<div class="article-p" style="padding-bottom: 50px;">
												<div>
													<h3><a href="<?php echo get_the_permalink($article['ID']); ?>"><?php echo apply_filters('the_title', $article['post_title']); ?></a></h3>
												</div>
											</div>
										</div>
									</div>
								<?php endforeach; ?>
							</div>

						<?php endif; ?>
					</div>

					<div class="commentary limit-width">
						<?php comments_template(); ?>
					</div>

				</div>

		<?php get_sidebar(); ?>

	</div>

</div>

<?php get_footer(); ?>
