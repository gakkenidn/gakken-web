		<footer class="footer">
			<section id="footer">
				<div class="container full-width">
					<div class="row">

						<div class="col-sm-4 hidden-sm hidden-xs">
							<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-full-web65.png" style="width:100%;max-width:200px;" />
							<p class="small" style="margin-top:15px;">
								PT. Gakken Health and Education Indonesia<br />
								Jl. Dr. Ratulangi C9 No.7<br />
								Makassar, Indonesia
							</p>

							<div style="padding:10px 0px;">
								<h5>Support:</h5>
								<p class="small">support@gakken-idn.co.id</p>
								<p class="small">or call <b style="font-weight:600;">0800-1-401652 (Toll Free)</b></p>
							</div>
						</div>

						<div class="col-md-8 col-sm-3 widget social">
							<a href="https://instagram.com/Gakken.IDN" class="fa fa-instagram" target="_blank" rel="nofollow"></a>
							<a href="https://twitter.com/GakkenIDN" class="fa fa-twitter" target="_blank" rel="nofollow"></a>
							<a href="http://www.facebook.com/GakkenIDN" class="fa fa-facebook" target="_blank" rel="nofollow"></a>
						</div>

						<?php dynamic_sidebar('footer'); ?>


						<div class="col-sm-3 hidden-sm hidden-xs">
							<h3>Newsletter</h3>
							<form class="form-inline" action="<?php echo get_option('gakken_accounts_base_url', '') . 'api/subscribe-email'; ?>" accept-charset="utf-8" method="post" id="newsletter-subscribe" style="margin-top: 5px;">
								<div class="form-group">
									<input type="email" name="email" class="form-control" placeholder="Alamat email" />
									<button type="button" id="newsletter-subscribe-btn" class="btn highlight-btn">Daftar</button>
								</div>
							</form>
							<p style="font-size: 12px;margin-top: 5px;" id="newsletter-subscribe-pre-message">Daftarkan email Anda untuk terus mendapatkan informasi terbaru dari Gakken Indonesia.</p>
							<p class="hidden" style="font-size: 12px;margin-top: 5px;" id="newsletter-subscribe-post-message">Terima kasih telah mendaftar newsletter Gakken Indonesia!</p>
						</div>

					</div>

					<div class="small text-center" style="margin-top:20px;">
						Copyright 2015 &copy; PT. Gakken Health and Education Indonesia. All Rights Reserved.
					</div>
				</div>
			</section>
		</footer>

		<?php wp_footer(); ?>

		<script type="text/javascript">
			<?php $vault_url = do_shortcode('[gkvault-get-base-url]'); ?>
	        var url_fcm = "<?= $vault_url . 'api/fcm/save' ?>";
	    </script>
		<script src="<?php echo esc_url(get_template_directory_uri()); ?>/javascripts/bootstrap.min.js" crossorigin="anonymous"></script>
		<script src="<?php echo esc_url(get_template_directory_uri()); ?>/javascripts/bootstrap-sprockets.js" crossorigin="anonymous"></script>
		<script src="<?php echo esc_url(get_template_directory_uri()); ?>/javascripts/bootstrap/tooltip.js" crossorigin="anonymous"></script>
		<script src="<?php echo esc_url(get_template_directory_uri()); ?>/javascripts/app.js?ver=1.1" crossorigin="anonymous"></script>
		<script src="<?php echo esc_url(get_template_directory_uri()); ?>/javascripts/active-menu.js" crossorigin="anonymous"></script>
		<script src="<?php echo esc_url(get_template_directory_uri()); ?>/javascripts/push-notif.js"></script>
		<script id="dsq-count-scr" src="//gakken-indonesia.disqus.com/count.js?https" async></script>
		<?php do_shortcode('[gakken-accounts-script]'); ?>
	</body>
</html>
