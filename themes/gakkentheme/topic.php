<?php
    /*Template Name : Topik page */


    get_header();
    // single topic
    if($wp_query->query_vars['topic'] != 'all'):
        $slug = urldecode($wp_query->query_vars['topic']);

        $topic               = json_decode(do_shortcode('[gkvault-topics-single slug=' . $slug . ']'));
        $account             = json_decode( do_shortcode('[gkaccount-login-status]') );
        $date_today          = date('Y-m-d H:i:s');
        $subscription_status = false;
        $vaulturl            = do_shortcode('[gkvault-get-base-url]');

        if ($account->status == 'authenticated' && $account->subscription) {
            $subscription_status = $account->subscription->expired_at > $date_today;
        }
?>

    <div class="full-width container-fluid">
        <div class="main-container" style=" margin-top: 20px; ">
            <aside class= "sidebar-container topics-only col-sm-3">
                <div class="topics-tab" id="sidebar-scroll" data-spy="affix" data-offset-top="197">
                    <div class="list-group" role="tablist">
                        <a href="#overview" class="list-group-item">
                            <span class="description"> Rangkuman </span> <span class="fa fa-caret-right"> </span>
                        </a>
                        <a href="#lecture" class="list-group-item">
                            <span class="description"> Pengajar </span> <span class="fa fa-caret-right"> </span>
                        </a>
                        <?php if (!empty($topic->preview)): ?>
                            <a href="#preview" class="list-group-item">
                                <span class="description"> Preview </span> <span class="fa fa-caret-right"> </span>
                            </a>
                        <?php endif; ?>
                        <a href="#content-summary" class="list-group-item">
                            <span class="description"> Konten <span class="fa fa-caret-right"> </span> </span>
                        </a>
                        <a href="#achievements" class="list-group-item">
                            <span class="description"> Benefit <span class="fa fa-caret-right"> </span> </span>
                        </a>
                        <a href="#topic-faq" class="list-group-item">
                            <span class="description"> FAQs </span> <span class="fa fa-caret-right"> </span>
                        </a>
                        <?php if( !$subscription_status): ?>
                            <div class="list-group-item">
                                <span class="description">
                                    <button type="button" class="btn btn-lg btn-primary" onclick="javascript:location.href='<?= bloginfo('url') ?>/berlangganan';"> Subscribe </button>
                                </span>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </aside>

            <div class="article-wrapper col-sm-9">
                <div class="single-article-container topic-article" style="overflow: hidden;">
                    <h1 class="title"> <?= $topic->title ?> </h1>

                    <section id="overview">
                        <h2 class="divider"> Rangkuman </h2>

                        <article>

                            <?= $topic->summary ?>

                        </article>

                    </section>

                    <section id="lecture">
                        <h2 class="divider"> Pengajar </h2>

                          <div class="taught-by col-sm-12">
                            <h4> Mentor  </h4>
                        <?php foreach ($topic->lecture as $lecture): ?>
                                <div class="taught-by-wrapper">
                                <div class="taught-by-desc ">
                                    <h3> <?= $lecture->name ?> </h3>
                                    <?= $lecture->bio ?>
                                </div>
                                <?php if ($lecture->photo): ?>
                                    <div class="taught-by-pic ">
                                        <div class="taught-by-pic-container" style="background: url('<?= $vaulturl ?>/lecture/photo/<?= $lecture->photo ?>') no-repeat; background-size: cover; background-position: 50% 50%;"></div>
                                    </div>
                                <?php endif; ?>
                              </div>
                        <?php endforeach; ?>
                        </div>
                    </section>

                    <?php if (!empty($topic->preview)): ?>
                        <section id="preview">
                            <h2 class="divider"> Preview </h2>

                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="<?= $topic->preview ?>" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </section>
                    <?php endif; ?>

                    <section id="content-summary">
                        <h2 class="divider"> Konten </h2>
                        <p> Topik ini akan berisi : </p>

                        <div class="list-group">
                            <?php foreach ($topic->content as $content):
                                switch ($content->type) {
                                    case 'image': $icon = '<i class="fa fa-file-image-o"> </i>'; break;
                                    case 'video': $icon = '<i class="fa fa-play"> </i>'; break;
                                    case 'audio': $icon = '<i class="fa fa-headphones"> </i>'; break;
                                    default:      $icon = '<i class="fa fa-file-text-o"> </i>'; break;
                                }

                                $url = $subscription_status ? get_site_url(null, 'topik/' . $topic->slug . '/konten/' . $content->slug) : '#';
                                var_dump($content);
                            ?>
                                <?php if ( $content->type == 'link'):
                                    if (strpos($content->data, 'https://p2kb.gakken-idn.id/moodle/mod/') == 0)
                                        $content->data = 'https://p2kb.gakken-idn.id/moodle/auth/gakken/sso.php?redirect=' . urlencode($content->data);
                                    ?>
                                    <a href="<?= $subscription_status ? $content->data : '#' ?>" class="list-group-item">
                                        <span class="description"> <?= $icon ?> </i>&nbsp;&nbsp;<?= $content->title ?> </span>
                                    </a>
                                <?php else: ?>
                                    <a href="<?= $url ?>" class="list-group-item">
                                        <span class="description"> <?= $icon ?> </i>&nbsp;&nbsp;<?= $content->title ?> </span>
                                    </a>
                                <?php endif; ?>
                            <?php endforeach; ?>

                            <?php if ($topic->is_p2kb): ?>
                                <!-- <a href="#" class="list-group-item"> <i class="fa fa-question"> </i>&nbsp;&nbsp;
                                    <span class="description"> Tes Latihan Penanggulangan Tuberculosis di Indonesia </span>
                                </a> -->
                            <?php endif; ?>
                        </div>
                        <?php if ( !$subscription_status): ?>
                            <div class="">
                                * Subscribe <a href="<?= bloginfo('url') ?>/berlangganan">disini</a>, untuk melihat konten *
                            </div>
                        <?php endif; ?>
                    </section>

                    <?php if ($topic->is_p2kb): ?>
                        <section id="achievements">
                            <h2 class="divider"> Benefit </h2>
                            <div class="achievements-container col-sm-12">
                                <div class="achivements-item col-sm-4">
                                    <img src="<?= get_bloginfo('template_directory') ?>/images/sertificates_02.png">
                                    <p class="description"> Sertifikat yang dapat ditukar dengan 2 SKP Points jika menyelesaikan tes </p>
                                </div>
                            </div>
                        </section>
                    <?php endif; ?>

                    <section id="topic-faq">
                        <h2 class="divider"> Frequently Asked Questions </h2>

                        <div class="accordion" id="accordion2">
                            <?php $i = 1; foreach ($topic->faq as $data_faq): ?>
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse<?= $i ?>">
                                            <i class="fa fa-caret-right"> </i>  <?= $data_faq->question ?>
                                        </a>
                                    </div>

                                    <div id="collapse<?= $i ?>" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <?= $data_faq->answer ?>
                                        </div>
                                    </div>
                                </div>
                            <?php $i++; endforeach; ?>

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1">
                                            <i class="fa fa-caret-right"> </i>  Q1: Bagaimana saya bisa mengakses topik?
                                        </a>
                                    </div>

                                    <div id="collapse1" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            Topik hanya dapat diakses oleh member yang telah melakukan pendaftaran dan pembayaran untuk berlangganan layanan Gakken P2KB.
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse2">
                                            <i class="fa fa-caret-right"> </i> Q2: Apa yang akan saya dapatkan ketika sudah berlangganan layanan Gakken P2KB?
                                        </a>
                                    </div>

                                    <div id="collapse2" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            Anda memiliki akses penuh di website Gakken Indonesia atas semua topik pembelajaran yakni berupa video dan teks, artikel dan jurnal medis, indeks obat, dokter, rumah sakit dan klinik.
                                    </div>
                                </div>

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse3">
                                            <i class="fa fa-caret-right"> </i> Q3: Apakah ada sesi trial sebelum membeli layanan penuh?
                                        </a>
                                    </div>

                                    <div id="collapse3" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            Ya. Tersedia Konten Trial yang bisa diakses tanpa keanggotaan. Hubungi contact support kami untuk mendapatkan akses trial. support@gakken-idn.co.id atau telepon di 0800-1-401652 (Toll Free)
                                    </div>
                                </div>

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse4">
                                            <i class="fa fa-caret-right"> </i> Q4: Berapa lama masa berlangganan?
                                        </a>
                                    </div>

                                    <div id="collapse4" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            Masa berlangganan layanan Gakken P2KB yaitu selama 365 hari atau 12 bulan terhitung dari mulai masa aktif berlangganan.
                                    </div>
                                </div>

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse5">
                                            <i class="fa fa-caret-right"> </i> Q5: Kapan topik terbaru rilis?
                                        </a>
                                    </div>

                                    <div id="collapse5" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            Topik pembelajaran terbaru dirilis pada tanggal 1 setiap bulannya.
                                    </div>
                                </div>

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse6">
                                            <i class="fa fa-caret-right"> </i> Q6: Kapan sertifikat bisa diunduh?
                                        </a>
                                    </div>

                                    <div id="collapse6" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            Sertifikat otomatis dapat diunduh ketika anda telah menyelesaikan tes dengan presentasi kelulusan minimal 80%.
                                    </div>
                                </div>

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse7">
                                            <i class="fa fa-caret-right"> </i> Q7: Bagaimana jika saya sudah mengikuti tes tapi hasilnya belum mencapai standar kelulusan 80%?
                                        </a>
                                    </div>

                                    <div id="collapse7" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            Anda bisa mengikuti tes berkali-kali hingga mencapai standar kelulusan selama masih dalam masa berlangganan.
                                    </div>
                                </div>

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse8">
                                            <i class="fa fa-caret-right"> </i> Q8: Bisakah saya mengunduh konten yang ada di dalam topik pembelajaran?
                                        </a>
                                    </div>

                                    <div id="collapse8" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            Anda hanya bisa mengakses semua topik yang telah dirilis selama terkoneksi dengan internet. Konten yang ada tidak bisa diunduh.
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>


<!-- //list topic -->
<?php else: ?>
    <div class="full-width container-fluid">
        <div class="main-container" >
            <div class="row" style="margin-left: 0px; margin-right: 0px; margin-top: 20px;">
                <div class="article-wrapper col-sm-9">

                    <div class="all-article-container" id="topic-list">
                        <!-- topic will be dynamically using dom -->
                    </div>

                    <div class="load-more-section">
                        <button type="button" id="load-more-btn" class="hidden btn btn-default btn-load-more"><i class='fa fa-circle-o-notch fa-spin hidden' id='loading-content'></i> Load More</button>
                    </div>

                </div>

                <?php get_sidebar();?>
            </div>
        </div>
    </div>

    <?php do_shortcode('[gkvault-topics-list]'); ?>
<?php endif; ?>

<?php get_footer();?>
