<?php
if (post_password_required()) {
	return;
}
?>

<div id="comments" class="comments-area single-article-container" style="min-height:auto;">

	<?php if (have_comments()) : ?>
		<h2 class="comments-title">
			<?php
				$comments_number = get_comments_number();
				if (1 === $comments_number) {
					printf(_x('One thought on &ldquo;%s&rdquo;', 'comments title', 'twentysixteen'), get_the_title());
				} else {
					printf(
						_nx(
							'%1$s thought on &ldquo;%2$s&rdquo;',
							'%1$s thoughts on &ldquo;%2$s&rdquo;',
							$comments_number,
							'comments title',
							'twentysixteen'
						),
						number_format_i18n($comments_number),
						get_the_title()
					);
				}
			?>
		</h2>

		<?php the_comments_navigation(); ?>

		<ol class="comment-list">
			<?php
				wp_list_comments(array(
					'style'       => 'ol',
					'short_ping'  => true,
					'avatar_size' => 42,
				));
			?>
		</ol>

		<?php the_comments_navigation(); ?>

	<?php endif; ?>

	<?php
		if (! comments_open() && get_comments_number() && post_type_supports(get_post_type(), 'comments')) :
	?>
		<p class="no-comments"><?php _e('Comments are closed.', 'twentysixteen'); ?></p>
	<?php endif; ?>

	<?php
	$commenter = wp_get_current_commenter();
	$req = get_option('require_name_email');
	$aria_req = ($req ? " aria-required='true'" : '');
	$fields = array(
		'email' => 
			'<div class="form-group"><label class="control-label col-lg-3 col-sm-4"><span class="pull-left">' . __('Alamat Email', 'domainreference') . '*</span></label><div class="col-lg-6 col-sm-8"><input class="form-control" id="email" name="email" type="text" value="' . esc_attr( $commenter['comment_author_email']) .'" size="30"' . $aria_req . ' /></div></div>',
		'author' =>
		    '<div class="form-group"><label class="control-label col-lg-3 col-sm-4"><span class="pull-left">' . __('Nama Lengkap', 'domainreference') . '*</span></label><div class="col-lg-6 col-sm-8"><input class="form-control" id="author" name="author" type="text" value="' . esc_attr($commenter['comment_author']) . '" size="30"' . $aria_req . ' /></div></div>',
	    'url' =>
	        '<div class="form-group"><label class="control-label col-lg-3 col-sm-4"><span class="pull-left">' . __('Website', 'domainreference') . '</span></label><div class="col-lg-6 col-sm-8"><input class="form-control" id="url" name="url" type="text" value="' . esc_attr($commenter['comment_author_url']) . '" size="30" /></div></div>',
	);
	comment_form(array(
		'class_form' => 'form-horizontal',
		'class_submit' => 'btn btn-lg btn-primary',
		'comment_field' => '<div class="form-group"><label class="control-label col-lg-3 col-sm-4"><span class="pull-left">' . _x('Komentar', 'noun') . '*</span></label><div class="col-lg-9 col-sm-8"><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true" class="form-control"></textarea></div></div>',
		'comment_notes_before' => '<p>Silahkan mengisi data dibawah ini untuk meninggalkan komentar pada artikel ini.</p>',
		'label_submit' => 'Simpan Komentar',
		'title_reply' => __( 'Komentar Anda' ),
		'title_reply_to' => __( 'Komentar Anda untuk %s' ),
		'title_reply_before' => '<h2 id="reply-title" class="comment-reply-title">',
		'title_reply_after' => '</h2>',
		'fields' => apply_filters('comment_form_default_fields', $fields)
	));
	?>

</div>
