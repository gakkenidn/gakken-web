<?php
/**
 * The template used for displaying page content
 *
 */
?>

              <?php include('slider-single.php');?>

              <div class="row" style="margin-left: 0px; margin-right: 0px; margin-top: 20px;">
              <div class="article-wrapper col-sm-9">

                      <div class="all-article-container">

                        <?php

                            $big = 999999999;

                            $paged = ( get_query_var('paged') ) ? absint(get_query_var('paged')) : 1;

                            $args = array(
                              'post_type' => 'post',
                              'ignore_sticky_posts' => 1,
                              'posts_per_page' => 5,
                              'paged' => $paged
                            );

                            $query = new WP_Query( $args );

                         if( $query->have_posts() ) : while( $query -> have_posts() ) : $query->the_post();
                        ?>


                              <div class="all-article-wrapper">
                                <div class="row">
                                  <div class="img col-sm-4 img-responsive">
                                    <?php if( !the_post_thumbnail() ):?>

                                      <?php catch_that_image(); ?>

                                  <?php else:?>

                                      <?php the_post_thumbnail('medium'); ?>

                                  <?php endif;?>
                                </div>

                                    <div class="container col-sm-8">
                                      <h2> <a href="<?php the_permalink();?>"> <?php the_title();?> </h2> </a>
                                      

                                      <?php $subheading = get_post_meta($post->ID, 'subheading', true); ?>
                                          <?php if (!empty($subheading)): ?>
                                  
                                          <?php echo apply_filters('the_excerpt', $subheading); ?>
                                        
                                        
                                        <?php endif; ?>
                                      <div class="sub-desc"> <?php the_date(); ?> <i style="color: #A41E22; font-weight: bold;">  &nbsp;  &nbsp;  &nbsp;   By </i> <?php the_author_posts_link(); ?> <p style="text-align: right; "> <a href="<?php the_permalink();?>"> Continue Reading </a> </p> </div>

                                    </div>
                                  </div>
                                </div>

                              <?php endwhile; ?>
                            <?php endif;?>

                            <div class="pagination">
                          <?php echo paginate_links( array(
                          	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                          	'format' => '?paged=%#%',
                          	'current' => max( 1, get_query_var('paged') ),
                          	'total' => $query->max_num_pages
                          )); ?>
                        </div>




                        </div>
            </div>




          <?php get_sidebar();?>

            </div>

        </div>
