<?php

if(!isset($content_width))
	$content_width = 960;


function new_excerpt_more($more) {
	global $post;
	return '<a href="'. get_permalink($post->ID) . '"> ... Continue Reading </a>';
}

add_filter('excerpt_more', 'new_excerpt_more', 100);

if(!function_exists('shape_setup')):

	function shape_setup(){

		require(get_template_directory().'/inc/template-tags.php');

		require(get_template_directory().'/inc/tweaks.php');
		/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on Shape, use a find and replace
	 * to change 'shape' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'shape', get_template_directory() . '/languages' );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
		) );

		add_theme_support( 'post-formats', array(
			'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
		) );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for the Aside Post Format
	 */
	add_theme_support( 'post-formats', array( 'aside' ) );

		add_theme_support( 'post-thumbnails' );
	add_theme_support( 'title-tag' );

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'shape' ),
		'extra' => __('Footer 1', 'shape'),
	) );

	register_sidebar([
	'name'      => __('Footer'),
	'id'      => 'footer',
	'description' => 'Goes at the bottom of page. Max 4 widgets',
	'before_widget' => '<div class="col-md-2 col-sm-3 widget">',
	'after_widget'  => '</div>',
	'before_title'  => '<h3>',
	'after_title' => '</h3>'
	]);

	}
endif;

add_action('after_setup_theme', 'shape_setup');

function create_bootstrap_menu( $theme_location ) {
	if ( ($theme_location) && ($locations = get_nav_menu_locations()) && isset($locations[$theme_location]) ) {


		$menu = get_term( $locations[$theme_location], 'nav_menu' );
		$menu_items = wp_get_nav_menu_items($menu->term_id);
		$menu_list = '';

		foreach( $menu_items as $menu_item ) {
			if( $menu_item->menu_item_parent == 0 ) {

				$parent = $menu_item->ID;

				$menu_array = array();
				foreach( $menu_items as $submenu ) {
					if( $submenu->menu_item_parent == $parent ) {
						$bool = true;
						$menu_array[] = '<li><a href="' . $submenu->url . '">' . $submenu->title . '</a></li>' ."\n";
					}
				}
				if( isset($bool) && $bool == true && count( $menu_array ) > 0 ) {

					$menu_list .= '<li class="dropdown">' ."\n";
					$menu_list .= '<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">' . $menu_item->title . ' <span class="caret"></span></a>' ."\n";

					$menu_list .= '<ul class="dropdown-menu">' ."\n";
					$menu_list .= implode( "\n", $menu_array );
					$menu_list .= '</ul>' ."\n";

				} else {

					$menu_list .= '<li>' ."\n";
					$menu_list .= '<a href="' . $menu_item->url . '">' . $menu_item->title . '</a>' ."\n";
				}

			}

			// end <li>
			$menu_list .= '</li>' ."\n";
		}



	} else {
		$menu_list = '<!-- no menu defined in location "'.$theme_location.'" -->';
	}

	echo $menu_list;
}


function catch_that_image() {
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  // $first_img = $matches[1][0];
  $first_img = $matches[1];

  if(empty($first_img)) {
	$first_img = "/path/to/default.png";
  }
  return $first_img;
}

function pagination_bar() {
	global $wp_query;

	$total_pages = $wp_query->max_num_pages;

	if ($total_pages > 1){
		$current_page = max(1, get_query_var('paged'));

		echo paginate_links(array(
			'base' => get_pagenum_link(1) . '%_%',
			'format' => '/page/%#%',
			'current' => $current_page,
			'total' => $total_pages,
		));
	}
}

function custom_pagination($numpages = '', $pagerange = '', $paged='') {

  if (empty($pagerange)) {
	$pagerange = 2;
  }

  /**
   * This first part of our function is a fallback
   * for custom pagination inside a regular loop that
   * uses the global $paged and global $wp_query variables.
   *
   * It's good because we can now override default pagination
   * in our theme, and use this function in default quries
   * and custom queries.
   */
  global $paged;
  if (empty($paged)) {
	$paged = 1;
  }
  if ($numpages == '') {
	global $wp_query;
	$numpages = $wp_query->max_num_pages;
	if(!$numpages) {
		$numpages = 1;
	}
  }

  /**
   * We construct the pagination arguments to enter into our paginate_links
   * function.
   */
  $pagination_args = array(
	'base'            => get_pagenum_link(1) . '%_%',
	'format'          => '&paged=%#%',
	'total'           => $numpages,
	'current'         => $paged,
	'show_all'        => False,
	'end_size'        => 1,
	'mid_size'        => $pagerange,
	'prev_next'       => True,
	'prev_text'       => __('&laquo;'),
	'next_text'       => __('&raquo;'),
	'type'            => 'plain',
	'add_args'        => false,
	'add_fragment'    => ''
  );

  $paginate_links = paginate_links($pagination_args);

  if ($paginate_links) {
	echo "<nav class='custom-pagination'>";
	  echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
	  echo $paginate_links;
	echo "</nav>";
  }

}

function more_posts() {
  global $wp_query;
  return $wp_query->current_post + 1 < $wp_query->post_count;
}


// function add_meta_tags() {

// 	if(is_home() && is_front_page()):


// 	else :

// 			echo	'
// 				<meta name="meta_name" content="meta_value" />
// 				<meta name="title" content="'.get_the_title().'"/>
// 			 	<meta name="description" content="'.get_bloginfo('description').'"/>

// 			 <meta name="twitter:card" content="summary" />
// 			 <meta name="twitter:site" content="http://gakken-idn.id" />
// 			 <meta name="twitter:title" content="'.get_the_title().'" />
// 			 <meta name="twitter:description" content="'.get_the_excerpt($post->ID).'" />';

// 			 if(!has_post_thumbnail( $post->ID )) { //the post does not have featured image, use a default image
// 	 			$default_image=get_template_directory()."images/logo-gakken.jpg"; //replace this with a default image on your server or an image in your media library
// 	 			echo '<meta name="twitter:image" content="'.$default_image.'" />';
// 	 		}
// 	 		else{
// 	 			$thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
// 	 			echo '<meta name="twitter:image" content="'.esc_attr($thumbnail_src[0] ).'" />';
// 	 		}


// 		echo '
// 			 <meta property="og:type" content="article"/>
// 			 <meta property="og:title" content="'.get_the_title().'" />
// 			 <meta property="og:url" content="http://gakken-idn.id" />';


// 			 if(!has_post_thumbnail( $post->ID )) { //the post does not have featured image, use a default image
// 				 $default_image=get_template_directory()."images/logo-gakken.jpg"; //replace this with a default image on your server or an image in your media library
// 				 echo '<meta property="og:image" content="' . $default_image . '"/>';
// 			 }
// 			 else{
// 				 $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
// 				 echo '<meta property="og:image" content="' . esc_attr( $thumbnail_src[0] ) . '"/>';
// 			 }

// 		echo '
// 			 <meta property="og:description" content="'.get_the_excerpt($post->ID).'" />
// 			 <meta property="og:site_name" content="Gakken Health and Education" />
// 			 <meta property="fb:app_id" content="104610659935847" />

// 			 '
// 	;

// 	endif;

// }
// add_action('wp_head', 'add_meta_tags');


// function get_meta_descriptions(){
// 		if (is_single()) {
//
// 			echo '<meta name="Description" content="'.strip_tags(get_the_excerpt($post->ID)).'" />';
//
// 			//if homepage use standard meta description
// 			} else if(is_home() || is_page())  {
//
// 			echo '<meta name="Description" content="Gakken Health and Education">';
//
// 			//if category page, use category description as meta description
// 			} else if(is_category()) {
//
// 			echo '<meta name="Description" content="'.strip_tags(category_description(get_category_by_slug(strtolower(get_the_category()))->term_id)).'" />';
// }
// }

function wpb_set_post_views($postID) {
	$count_key = 'wpb_post_views_count';
	$count = get_post_meta($postID, $count_key, true);
	if($count==''){
		$count = 0;
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, '0');
	}else{
		$count++;
		update_post_meta($postID, $count_key, $count);
	}
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);


//Adding the Open Graph in the Language Attributes
function add_opengraph_doctype( $output ) {
	return $output . ' xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml"';
}

add_filter('language_attributes', 'add_opengraph_doctype');
add_action('init', 'yourpluginname_rewrite_rules');

function yourpluginname_rewrite_rules() {
	add_rewrite_rule( 'topik/?$', 'index.php?topic=all', 'top' );
  	add_rewrite_rule( 'topik/judul/([^/]+)/?$', 'index.php?topic=$matches[1]', 'top' );
  	add_rewrite_rule( 'topik/penyakit/?$', 'index.php?disease=all', 'top' );
  	add_rewrite_rule( 'topik/penyakit/([^/]+)/?$', 'index.php?disease=$matches[1]', 'top' );
  	add_rewrite_rule( 'topik/([^/]*)/konten/([^/]*)/?', 'index.php?topic=$matches[1]&content=$matches[2]', 'top' );
  	add_rewrite_rule( 'jurnal/?$', 'index.php?journal=all', 'top' );
  	add_rewrite_rule( 'jurnal/artikel/([^/]+)', 'index.php?journalarticle=$matches[1]', 'top' );
  	add_rewrite_rule( 'jurnal/([^/]+)', 'index.php?journal=$matches[1]', 'top' );
  	add_rewrite_rule( 'obat/?', 'index.php?drug=all', 'top' );
	add_rewrite_rule( 'mediadata/?' , 'index.php?media=all', 'top');
	add_rewrite_rule( 'video/([^/]+)', 'index.php?video=$matches[1]', 'top' );
  	// add_rewrite_rule( 'obat/search/?$', 'index.php?drug=search', 'top' );
  	// add_rewrite_rule( 'obat/([^/]+)', 'index.php?drug=$matches[1]', 'top' );
	flush_rewrite_rules();
}

add_filter( 'query_vars', 'yourpluginname_register_query_var' );
function yourpluginname_register_query_var( $vars ) {
  	$vars[] = 'disease';
	$vars[] = 'topic';
	$vars[] = 'journal';
  	$vars[] = 'content';
  	$vars[] = 'journalarticle';
  	$vars[] = 'drug';
	$vars[] = 'term';
	$vars[]	= 'media';
	$vars[] = 'video';
  	return $vars;
}

add_filter('template_include', 'yourpluginname_blah_template_include', 1, 1);
function yourpluginname_blah_template_include($template)
{
	global $wp_query;
	$diseae_value  	= isset($wp_query->query_vars['disease']) ? $wp_query->query_vars['disease'] : false;
	$topic_value   	= isset($wp_query->query_vars['topic']) ? $wp_query->query_vars['topic'] : false;
	$journal_value 	= isset($wp_query->query_vars['journal']) ? $wp_query->query_vars['journal'] : false;
	$content_value 	= isset($wp_query->query_vars['content']) ? $wp_query->query_vars['content'] : false;
	$journalarticle	= isset($wp_query->query_vars['journalarticle']) ? $wp_query->query_vars['journalarticle'] : false;
	$drug 			= isset($wp_query->query_vars['drug']) ? $wp_query->query_vars['drug'] : false;
	$term 			= isset($wp_query->query_vars['term']) ? $wp_query->query_vars['term'] : false;
	$media			= isset($wp_query->query_vars['media']) ? $wp_query->query_vars['media'] : false;
	$video			= isset($wp_query->query_vars['video']) ? $wp_query->query_vars['video'] : false;

	if ($diseae_value) {
		return plugin_dir_path(__FILE__).'/disease.php';
	} elseif ($content_value) {
		return plugin_dir_path(__FILE__).'/topic_content.php';
	} elseif ($topic_value) {
		if($topic_value == 'all') return plugin_dir_path(__FILE__).'/all-topik.php';
		else return plugin_dir_path(__FILE__) . 'single-topik.php';
	} elseif ($journal_value) {
		if ($journal_value == 'all') return plugin_dir_path(__FILE__).'/all-jurnal.php';
		else return plugin_dir_path(__FILE__) . '/jurnal-single.php';
	} elseif ($journalarticle) {
		return plugin_dir_path(__FILE__).'/jurnal-artikel.php';
	} elseif ($drug) {
		return plugin_dir_path(__FILE__).'/all-obat.php';
	} elseif ($media) {
		return plugin_dir_path(__FILE__).'/mediadata.php';
	} elseif ($video) {
		return plugin_dir_path(__FILE__).'/video-content.php';
	}

	return $template;
}

add_action( 'wpp_post_update_views', 'custom_wpp_update_postviews' );

function custom_wpp_update_postviews($postid) {
	// Accuracy:
	//   10  = 1 in 10 visits will update view count. (Recommended for high traffic sites.)
	//   30 = 30% of visits. (Medium traffic websites)
	//   100 = Every visit. Creates many db write operations every request.

	$accuracy = 100;

	if ( function_exists('wpp_get_views') && (mt_rand(0,100) < $accuracy) ) {
		// Remove or comment out lines that you won't be using!!
		update_post_meta( $postid, 'views_total',   wpp_get_views( $postid )            );
		update_post_meta( $postid, 'views_daily',   wpp_get_views( $postid, 'daily' )   );
		update_post_meta( $postid, 'views_weekly',  wpp_get_views( $postid, 'weekly' )  );
		update_post_meta( $postid, 'views_monthly', wpp_get_views( $postid, 'monthly' ) );
	}
}
?>
