<?php
    get_header();

    $query   = $wp_query->query_vars['disease'] != 'all' ? 'slug=' . $wp_query->query_vars['disease'] : '';
    $query_t = $wp_query->query_vars['disease'] != 'all' ? 'disease=' . $wp_query->query_vars['disease'] : '';
    $disease = json_decode(do_shortcode('[gkvault-disease-list ' . $query . ']'));
    $topics  = json_decode(do_shortcode('[gkvault-disease-topics-list ' . $query_t . ']'));

    function renderDisease( $disease , $topics , $margin ){

        foreach ($disease as $data) {
            echo "<li class='disease-click'> <i class='fa fa-folder'></i><i class='fa fa-folder-open hidden'></i>&nbsp;&nbsp;" . $data->name;

            if ( count ($data->child) > 0) {
                echo "<ul class='hidden'>";

                renderDisease( $data->child , [] , $margin + 10 );

                foreach ($data->topic as $topic) {
                    echo "<li class='topic-click'><a href='" . get_site_url(null, 'topik/judul/' . $topic->slug) . "'> <i class='fa fa-graduation-cap'></i>&nbsp;&nbsp;" . $topic->title . " </a></li>";
                }

                echo "<li> <a href='" . get_site_url(null, 'topik/penyakit/' . $data->slug) . "'> Selengkapnya... </a> </li>";
                echo "</ul>";
            }

            echo "</li>";
        }
        if ($topics) {
            foreach ($topics as $topic) {
                echo "<li class='topic-click'><a href='" . get_site_url(null, 'topik/judul/' . $topic->slug) . "'> <i class='fa fa-graduation-cap'></i>&nbsp;&nbsp;" . $topic->title . " </a></li>";
            }
        }

    }
?>

<div class="full-width container-fluid">
  <div class="row">
    <div class="main-container">

        <div class="article-wrapper col-sm-9" style="margin-top:20px;">
          <div class="search">
          <div class="input-group stylish-input-group">
          <input type="text" class="form-control"  placeholder="Search" >
          <span class="input-group-addon">
              <button type="submit">
                  <span class="fa fa-search"></span>
              </button>
          </span>

          </div>
          </div>

          <div class="disease">
                <ul>
                    <?php if ($disease) {
                        renderDisease($disease , $topics , 0);
                    } ?>
                </ul>
          </div>
        </div>

        <aside class= "sidebar-container col-sm-3">
          <div class="main-banner">
              <a href="#"> <img src="images/shutterstock.jpg"> </a>
          </div>

          <div class="sidebar-sub-container">
            <div class="list-group">
              <a href="#" class="list-group-item active">
                  Topik
              </a>
              <a href="#" class="list-group-item">

                <span class="description"> Cardiologi </span>
              </a>
              <a href="#" class="list-group-item"><span class="description"> Dermatologi </span> </a>
              <a href="#" class="list-group-item">
                <span class="description"> Endokrinologi </span>
              </a>
              <a href="#" class="list-group-item"><span class="description"> Gastroentorologi </span></a>
              <a href="#" class="list-group-item"><span class="description"> Hepatologi </span></a>
              <a href="#" class="list-group-item"><span class="description"> Penyakit Menular </span></a>
              <a href="#" class="list-group-item"><span class="description"> Neurologi </span></a>
              <a href="#" class="list-group-item"><span class="description"> Obstetrik & Ginekologi </span></a>
              <a href="#" class="list-group-item"><span class="description"> Onkologi </span></a>
              <a href="#" class="list-group-item"><span class="description"> Optalmologi </span></a>
              <a href="#" class="list-group-item"><span class="description"> Pedriatrik </span></a>
              <a href="#" class="list-group-item"><span class="description"> Psikiatri </span></a>
              <a href="#" class="list-group-item"><span class="description"> Respirologi </span></a>
              <a href="#" class="list-group-item"><span class="description"> Rheumatologi </span></a>
              <a href="#" class="list-group-item"><span class="description"> Urologi </span></a>
            </div>
          </div>

        </aside>

    </div>
  </div>
</div>

<?php get_footer(); ?>

<script type="text/javascript">
    var disease = document.querySelectorAll('li.disease-click')
        topic = document.querySelectorAll('li.topic-click');

    disease.forEach(function(ds){
        ds.addEventListener('click', function(e){
            var ul          = this.querySelector('ul') ,
                folderOpen  = this.querySelector('i.fa-folder') ,
                folderClose = this.querySelector('i.fa-folder-open');

            if (folderOpen)
                folderOpen.classList.toggle('hidden');

            if (folderClose)
                folderClose.classList.toggle('hidden');

            if (ul)
                ul.classList.toggle('hidden');


            e.stopPropagation();
        })
    });

    topic.forEach(function(dt){
        dt.addEventListener('click', function(e){
            e.stopPropagation();
        })
    });
</script>
