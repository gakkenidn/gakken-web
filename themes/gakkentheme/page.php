<?php
/*Template Name : All Posts */

?>

 <?php get_header();?>


    <?php if(is_page('berlangganan') ):

            // Include the page content template.
            get_template_part( 'content', 'subscribe' );

            // End of the loop.

    else: ?>

            <div class="full-width container-fluid">


              <div class="main-container" >



              <?php
          		// Start the loop.

              if(is_page('Artikel Terbaru')):

          			// Include the page content template.
          			get_template_part( 'content', 'page' );

          			// End of the loop.

                else:

                get_template_part('content', 'page-single');

              endif; ?>

              </div>
            </div>

      <?php endif;?>

    <?php get_footer();?>
