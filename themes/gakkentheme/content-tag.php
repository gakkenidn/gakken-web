<?php
/**
 * The template used for displaying tag content
 *
 */
?>

              <div class="article-wrapper col-sm-9">

                      <div class="all-article-container">

                        <?php

                            $big = 999999999;

                            $tag = get_queried_object();

                            $paged = ( get_query_var('paged') ) ? absint(get_query_var('paged')) : 1;

                            $args = array(
                              'tag' => $tag->name,
                              'post__not_in' => get_option( 'sticky_posts'),
                              'posts_per_page' => 5,
                              'paged' => $paged
                            );

                            $query = new WP_Query( $args );

                         if( $query->have_posts() ) : while( $query->have_posts() ) : $query->the_post();
                        ?>


                              <div class="all-article-wrapper">
                                <div class="row">
                                  <div class="img col-sm-4 img-responsive">
                                    <?php if( !the_post_thumbnail() ):?>

                                      <?php catch_that_image(); ?>

                                  <?php else:?>

                                      <?php the_post_thumbnail('medium'); ?>

                                  <?php endif;?>
                                </div>

                                    <div class="container col-sm-8">
                                      <h2> <a href="<?php the_permalink();?>"> <?php the_title();?> </h2> </a>
                                      <div class="sub-desc"> <?php the_date($post_ID); ?> | By <?php the_author_posts_link(); ?> </div>

                                      <?php the_excerpt();?>

                                    </div>
                                  </div>
                                </div>

                              <?php endwhile; ?>
                            <?php endif;?>

                            <?php if($query->have_posts() > 5):?>
                            <div class="pagination">
                              <?php echo paginate_links( array(
                              	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                              	'format' => '?paged=%#%',
                              	'current' => max( 1, get_query_var('paged') ),
                              	'total' => $query->max_num_pages
                              )); ?>
                            </div>
                            <?php endif;?>

                        </div>
            </div>
