<div class="slider-container full-width container-fluid">

  <section class="slide-wrapper col-sm-9">
    <div class="slider">

     <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/javascripts/slider.js"></script>

     <div class="ninja-wrap">
        <section class="slider-cont">

          <?php $query = new WP_Query( array( 'posts_per_page' => 5, 'post__not_in' => get_option( 'sticky_posts') ) );

           if( $query->have_posts() ) : while( $query -> have_posts() ) : $query->the_post();
          ?>
              <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>


             

              <div class="slide" style="overflow:hidden; " >
                           <a href="<?php the_permalink();?>" alt="<?php the_title();?>" style="display:block; background-image: url('<?php echo $thumb['0'];?>');
                                  background-size: cover; background-repeat: no-repeat;
                                  background-position: center; height: 100%;
                                  width: 100%; ">
                             <!-- <img src='<?php echo $thumb['0'];?>' /> -->
                           </a>

                 <div class="container" style="position: absolute; bottom: 0; height: 30% !important;">
                      <section class="row main-width">
                          <section class="slide-copy">
                            <article>
                              <h1><a href="<?php the_permalink();?>" alt="<?php the_title();?>"> <?php the_title();?> </a> </h1>

                                <?php the_excerpt();?>


                              </article>
                              </section>
                     </section>
                 </div>
             </div>


           <?php endwhile; endif; ?>

        </section>
     </div>

      <div class="slider-nav main-width">
      <div class="slider-nav-wrapper">
        <ul class="slider-dots">
           <li><a href="#" class="arrow-prev"><i class="fa fa-angle-left"></i></a></li>

           <?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();
           ?>
              <li class="dot"><i class="fa fa-circle fa-1"></i></li>

          <?php endwhile; endif;?>

          <li> <a href="#" class="arrow-next"><i class="fa fa-angle-right"></i></a></li>
        </ul>
      </div>
      </div>
      </div>
  </section>

  <div class="slider-sidebar col-sm-3">


    <?php if ( have_posts() && query_posts('showposts=3') ) : while ( have_posts() ) : the_post();
    ?>

        <?php if( is_sticky() ):?>


            <div class="small-sidebar">

              <div class="img">
                <a href="<?php the_permalink();?>"> <?php the_post_thumbnail('medium'); ?> </a>
              </div>

               <div class="container">
                     <h1><a href="<?php the_permalink();?>" title="<?php the_title();?>"> <?php the_title();?> </a> </h1>
               </div>
              </div>

  <?php endif;?>
  <?php endwhile; endif;?>

  </div>
</div>

<div class="article-wrapper col-sm-9">
<div class="article-container artikel">
  <h1> Artikel Terbaru <span class="readmore">
    <a href="index.php/artikel-terbaru"> Read More </a>
  </span>
  </h1>

    <div class="row">
      <?php $i=0; $query = new WP_Query( array( 'posts_per_page' => 9, 'ignore_sticky_posts' => 1 ) );

       if( $query->have_posts() ) : while( $query -> have_posts() ) : $query->the_post();
      ?>

      <?php if ( $i % 3 === 0 ): ?>
         </div>
         <div class="row">
      <?php endif; ?>

      <div class=" article-row col-sm-4">
        <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
        <a href="<?php the_permalink();?>" title="<?php the_title();?>">
          <div class="article-img" style="background: url('<?php echo $thumb['0'];?>') no-repeat; background-size: cover;background-position: 50% 50%;"></div>
        </a>
        <h3> <a href="<?php the_permalink();?>" alt="<?php the_title();?>"> <?php the_title();?> </a> </h3>
      </div>
    <?php $i++; endwhile; endif;?>
    </div>

  </div>


  <?php if (false): ?>
<div class="article-container">
    <h1> Topik Terbaru <span class="readmore"> <a href="#"> Read More </a> </span> </h1>

    <div class="row">
      <div class=" article-row col-sm-6">
        <div class="article-img">
          <span class="category-highlight"> Neurologi </span>
        </div>
        <h3> <a href="#"> This Exercise Works Your Glutes, Hamstrings, and Lower Back All At Once </a> </h3>

      </div>

      <div class=" article-row col-sm-6">
        <div class="article-img">
          <span class="category-highlight"> Pediatrik </span>
        </div>

        <h3> <a href="#"> The 10-Minute Transformation </a> </h3>

      </div>
    </div>

    <div class="row">
      <div class=" article-row col-sm-6">
        <div class="article-img">
          <span class="category-highlight"> Neurologi </span>
        </div>
        <h3> <a href="#"> This Exercise Works Your Glutes, Hamstrings, and Lower Back All At Once </a> </h3>

      </div>

      <div class=" article-row col-sm-6">
        <div class="article-img">
          <span class="category-highlight"> Pediatrik </span>
        </div>

        <h3> <a href="#"> The 10-Minute Transformation </a> </h3>

      </div>
    </div>

</div>
  <?php endif; ?>

<div class="article-container">
  <div class="row">
    <div class="two-tab col-sm-6">
      <?php do_shortcode('[gkvault-drugs-list]'); ?>

    </div>

    <?php if(false): ?>
    <div class="two-tab col-sm-6">
      <h1> Jurnal Terbaru <span class="readmore"> <a href="#"> Read More </a> </span> </h1>

      <div class="list-group">
        <a href="#" class="list-group-item">
          Professional assistance during birth and maternal mortality in two Indonesian districts <br><br>
          <span class="description"> C. Ronsmans, S. Scott, S.N. Qomariyah, E. Achadi, D. Braunholtz, T. Marshall, E. Pambudi, K.H. Wittenc & W.J. Graham </span>
        </a>
        <a href="#" class="list-group-item">Acetylcholine<br><br>
          <span class="description"> Mata / Miotik </span> </a>
        <a href="#" class="list-group-item">
          Acetylcysteine <br><br>
          <span class="description"> Antidot &amp; Agen Detoksifikasi / Antidot, Agen Detoksifikasi &amp; Obat dalam Dependensi Substansi / Mata / Preparat Batuk &amp; Demam / Preparat Mata Lainnya / Sistem Pernapasan </span>
        </a>
        <a href="#" class="list-group-item">Acetylcholine<br><br>
          <span class="description"> Mata / Miotik </span> </a>
      </div>

    </div>
    <?php endif; ?>

  </div>
</div>

</div>
