<div class="slider-container full-width container-fluid">

    <section class="slide-wrapper">
        <div class="slider">
            <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/javascripts/slider.js"></script>

            <div class="ninja-wrap">
                <section class="slider-cont">

                    <?php
                    $images = [
                        ['href' => get_bloginfo('url') . '/topik', 'url' => get_template_directory_uri() . '/images/banner-web-03.jpg'],
                        ['href' => get_bloginfo('url') . '/jurnal', 'url' => get_template_directory_uri() . '/images/banner-web-01.jpg'],
                        ['href' => get_bloginfo('url') . '/artikel-terbaru', 'url' => get_template_directory_uri() . '/images/banner-web-04.jpg'],
                    ];
                    foreach($images as $image):
                        ?>
                        <div class="slide" style="overflow:hidden;">
                            <a class="slide-a" href="<?php echo $image['href']; ?>" style="
                                display:block; 
                                background-image: url('<?php echo $image['url']; ?>');
                                background-size: cover;
                                background-repeat: no-repeat;
                                background-position: center;
                                height: 100%;
                                width: 100%;
                                ">
                            </a>
                        </div>
                    <?php
                    endforeach;
                ?>
                </section>
            </div>

            <div class="slider-nav main-width">
                <div class="slider-nav-wrapper">
                    <ul class="slider-dots">
                        <li><a href="#" class="arrow-prev"><i class="fa fa-angle-left"></i></a></li>

                        <?php for($i = 0; $i < count($images); $i++): ?> 
                            <li class="dot"><i class="fa fa-circle fa-1"></i></li>
                        <?php endfor;?>

                        <li><a href="#" class="arrow-next"><i class="fa fa-angle-right"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</div>