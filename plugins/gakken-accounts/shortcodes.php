<?php

// Shortcodes
function gakken_accounts_account_shortcode()
{
	$baseUrl = gakken_accounts_get_base_url();
	$requestUrl = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	$sendCookies = [];

	if (isset($_COOKIE['gksession'])) $sendCookies['gksession'] = $_COOKIE['gksession'];
	$request = wp_remote_get($baseUrl . 'api/status?request_url=' . urlencode($requestUrl), ['cookies' => $sendCookies]);
	if (!is_array($request))
		$gakken_accounts_user_instance = false;
	else
		$gakken_accounts_user_instance = json_decode($request['body']);

	if ($gakken_accounts_user_instance) {
		if ($gakken_accounts_user_instance->status == "authenticated") {
			?>
			<div class="navbar-right">
				<ul class="nav navbar-nav" data-gakken-accounts="account-wrapper">
				  	<li class="dropdown user-profile-wrap">
				    	 
			    		<button href="#" class="dropdown-toggle user-profile-wrap-btn" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span style="float: left;padding: 7px 10px;"><?php echo $gakken_accounts_user_instance->name ?></span>
				    		<div class="user-profile" style="background: url('<?php echo $gakken_accounts_user_instance->avatar_url ?: $baseUrl . 'assets/img/placeholder_avatar.png'; ?>') no-repeat; background-size: cover; background-position: 50% 50%;float: left;"></div>
				    	</button>
				    	<ul class="dropdown-menu user-tab">
					      	<li><a href="<?php echo $baseUrl . 'my'; ?>">Akun Saya</a></li>
					      	<li><a href="<?php echo $baseUrl . 'my/settings'; ?>">Pengaturan</a></li>
					      	<li role="separator" class="divider"></li>
					      	<li><a href="<?php echo $baseUrl . 'logout'; ?>">Keluar</a></li>
				    	</ul>
				  	</li>
				</ul>
			</div>
			<?php
			return;
		}
	}

	$currentURL = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	?>
	<div class="navbar-right" style="margin-top:10px;">
		<div data-gakken-accounts="account-wrapper">
		  	<a href="#" class="btn btn-default navbar-btn highlight-btn" data-gakken-accounts="sign-in-btn" data-toggle="modal" data-target="#signin">Sign in</a>
		</div>
	</div>
	<?php

	return;
}

function gakken_accounts_signin_shortcode()
{
	$baseUrl = gakken_accounts_get_base_url();
	$currentURL = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	?>

	<div class="modal fade" id="signin" role="dialog" data-gakken-accounts="sign-in-form">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<a href="#" data-dismiss="modal" class="modal-x">&times;</a>
	        		<h4 class="modal-title">Sign In </h4>
	      		</div>
	      		<div class="modal-body">
	        		<div class="login-wrapper">
	          			<div class="login-form">
	            			<h3>Masuk ke Akun Anda</h3>
	            			<h4>Tidak memiliki akun? <a href="<?php echo $baseUrl . 'register'; ?>">Daftar</a></h4>
	            			<div class="login-container">
	              				<form accept-charset="utf-8" method="post" action="<?php echo $baseUrl . 'login'; ?>">
	              					<input type="hidden" name="redirect" value="<?php echo "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" />
	                				<div class="form-group">
	                  					<label>Alamat Email</label>
	                  					<input name="email" type="email" class="form-control" placeholder="Email">
	                				</div>
	                				<div class="form-group">
	                  					<label>Kata Sandi</label>
	                  					<input name="password" type="password" class="form-control" placeholder="Password">
	                				</div>
	                				<div class="form-group">
	                  					<input name="remember_me" type="checkbox"> Ingat saya</input>
	                				</div>
				                	<button type="submit" class="btn btn-default btn-block">Masuk</button>
	              				</form>

	              				<hr />

	              				<div>
									<a href="<?php echo $baseUrl . 'oauth/google?ref=modal&redirect=' . urlencode($currentURL); ?>" class="btn btn-block" style="background: #ffffff; border-radius: 2px; box-shadow: 1px 1px 4px #999; color: #333333 !important; font-weight: 700; margin-bottom: 5px; padding-top: 10px; padding-bottom: 10px; position: relative; text-transform: uppercase;">
										<img src="<?php echo $baseUrl . 'assets/img/google/google_signin_logo.png'; ?>" style="position: absolute; left: 15px; top: 5px; height: 30px; width: 30px;" />
										Masuk dengan Google
									</a>
									<a href="<?php echo $baseUrl . 'oauth/facebook?ref=modal&redirect=' . urlencode($currentURL); ?>" class="btn btn-block btn-facebook" style="background: #3b5998; border-radius: 2px; box-shadow: 1px 1px 4px #666; color: #ffffff !important; font-weight: 700; padding-top: 10px; padding-bottom: 10px; position: relative; text-transform: uppercase;">
										<img src="<?php echo $baseUrl . 'assets/img/facebook/facebook_signin_logo.png'; ?>" style="position: absolute; left: 15px; top: 5px; height: 30px; width: 30px;" />Masuk dengan Facebook
									</a>
								</div>
	            			</div>
	          			</div>
	        		</div>
	      		</div>
    		</div>
	  	</div>
	</div>
	<?php
}

function gakken_accounts_script_shortcode()
{
	?>
	<script type="text/javascript">
		$(document).ready(function () {
			var _accountWrapper = $("[data-gakken-accounts=account-wrapper]");
			var _signInBtn = $("[data-gakken-accounts=sign-in-btn]");
			var _signInModal = $("[data-gakken-accounts=sign-in-form]");
			var _signInForm = $("[data-gakken-accounts=sign-in-form] form");
			if (_accountWrapper.length) {
				_accountWrapper.clone().prependTo($("[data-gakken-accounts=account-wrapper-additional]"));
			}
			if (!_signInBtn.length) {
				_signInModal.remove();
			} else {
				_signInModal.on('shown.bs.modal', function() {
					$(this).find('input[name=email]').focus();
					_signInBtn.blur();
				}).on('hidden.bs.modal', function () {
					$(this).find('input').val('');
				});
			}
		});
	</script>
	<?php

}

// Register shortcodes
add_action('init', 'gakken_accounts_register_shortcodes');
function gakken_accounts_register_shortcodes()
{
	add_shortcode('gakken-account', 'gakken_accounts_account_shortcode');
	add_shortcode('gakken-signin', 'gakken_accounts_signin_shortcode');
	add_shortcode('gakken-accounts-script', 'gakken_accounts_script_shortcode');
}