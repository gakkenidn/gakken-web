$(document).ready(function () {
	var _accountWrapper = $("[data-gakken-accounts=account-wrapper]");
	var _signInBtn = $("[data-gakken-accounts=sign-in-btn]");
	var _signInModal = $("[data-gakken-accounts=sign-in-form]");
	var _signInForm = $("[data-gakken-accounts=sign-in-form] form");
	if (_accountWrapper.length) {
		_accountWrapper.clone().prependTo($("[data-gakken-accounts=account-wrapper-additional]"));
	}
	if (!_signInBtn.length) {
		_signInModal.remove();
	} else {
		_signInModal.on('shown.bs.modal', function() {
			$(this).find('input[name=email]').focus();
			_signInBtn.blur();
		}).on('hidden.bs.modal', function () {
			$(this).find('input').val('');
		});
	}
});