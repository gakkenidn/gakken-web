<?php

/*
Plugin Name: Gakken Accounts
Description: Authentication extension to the Gakken Accounts app
Author: Gakken Indonesia IT Team
License: GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/

require_once('settings.php');
require_once('shortcodes.php');

// Front-end page footer
// add_action('wp_footer', 'gakken_accounts_append_scripts');
// function gakken_accounts_append_scripts()
// {
// 	$scripts = ['main.js'];
// 	foreach ($scripts as $script) {
// 		?/><script type="text/javascript" src="</?php echo bloginfo('url'); ?/>/wp-content/plugins/gakken-accounts/js/<?php echo $script; ?/>"></script></?php
// 	}
// }

function gakken_accounts_get_base_url()
{
	$baseUrl = get_option('gakken_accounts_base_url', '');
	if (substr($baseUrl, -1) != '/') // check for trailing slash
		$baseUrl = $baseUrl . '/';

	return $baseUrl;
}